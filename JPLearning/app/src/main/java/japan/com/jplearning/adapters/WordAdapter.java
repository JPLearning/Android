package japan.com.jplearning.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import japan.com.jplearning.R;

/*
 * Created by HK on 24/10/2017.
 */

public class WordAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    List<DataBind> list;

    public WordAdapter(List<DataBind> dataBinds) {
        this.list = dataBinds ;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            //inflate your layout and pass it to view holder
            View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.words_header_layout,parent,false);
            return new VHHeader(view);
        } else if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            View view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.word_item_layout,parent,false);
            return new VHItem(view);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof VHItem) {
            String dataItem = getItem(position);
            ((VHItem) holder).title.setText(dataItem);
            ((VHItem) holder).checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    list.get(position).isChecked = isChecked;
                }
            });
            //cast holder to VHItem and set data
        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
        }
    }
    public List<String> getItemChecked(){

        List<String> mlist = new ArrayList<>();
        for (DataBind dataBind : list){
            if(dataBind.isChecked)
                mlist.add(dataBind.data);
        }
        return mlist;
    }

    @Override
    public int getItemCount() {
        return list.size() ;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == -1;
    }

    private String getItem(int position) {
        return list.get(position).data;
//                data[position - 1];
    }

    public static class DataBind {
        String data;
        public boolean isChecked;
        public DataBind(String text){
            data = text;
            isChecked = false;
        }
    }
    class VHItem extends RecyclerView.ViewHolder {

        TextView title;
        CheckBox checkBox;

        public VHItem(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.text);
            checkBox = itemView.findViewById(R.id.checkbox);


        }
    }

    class VHHeader extends RecyclerView.ViewHolder {

        Button button;

        public VHHeader(View itemView) {
            super(itemView);
        }
    }
}