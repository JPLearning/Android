package japan.com.jplearning.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/*
 * Created by HK on 19/10/2017.
 */
public class CustomVideoView extends VideoView {


    private int _overrideWidth,_overrideHeight;

    private PlayPauseListener mListener;

    public CustomVideoView(Context context) {
        super(context);
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    /*
        Resize video
     */

    public void resizeVideoView(int width, int height){

        _overrideHeight = height;
        _overrideWidth = width;
        // not sure whether it is useful or not but safe to do so
        getHolder().setFixedSize(width, height);
        //getHolder().setSizeFromLayout();
        requestLayout();
        invalidate();//call onmeasure here
    }
    //TODO TIEN PRO Recheck correct
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec,heightMeasureSpec);
    }

    public void setPlayPauseListener(PlayPauseListener listener) {
        mListener = listener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.onPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.onPlay();
        }
    }

    public static interface PlayPauseListener {
        void onPlay();
        void onPause();
    }

}
