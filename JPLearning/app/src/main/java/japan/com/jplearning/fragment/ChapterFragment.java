package japan.com.jplearning.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import japan.com.jplearning.ChapterActivity;
import japan.com.jplearning.PlayVideoActivity;
import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.models.Chapter;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mac on 10/20/17.
 */

public class ChapterFragment extends Fragment {


    ListView mListView;
    List<LessonLevel1> mListBook;
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        Map<String,String> map = new HashMap<>();
//        map.put("id","1");
//        APIUtils.getMarcoService().getListChapters(map).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                if (response.code()==200) {
//                    //Log.e("Data","Json :" + response.body().get("data").getAsString());
//                    JsonArray jsonArray = response.body().getAsJsonArray("data").getJ(0);
//                    AppData.mChapters = new ArrayList<Chapter>();
//                    for (int i= 0;i<jsonArray.size();i++){
//                        Chapter chapter = Chapter.fromJson(jsonArray.get(i).toString());
//                        AppData.mChapters.add(chapter);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//
//            }
//        });
//    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_information_main,
                container, false);

        mListView = (ListView) view.findViewById(R.id.listMainInfor);
        loadBookData();
        mListView.setAdapter(new ChapterFragment.LessonAdapter());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mListBook.get(position).hasContent){
                    AppData.currentChapter = position;
//                    ((ChapterActivity) getActivity()).chapter_id = position;
                    String chapterName = null;
                    switch (position){
                        case 0:
                            chapterName= getString(R.string.chapter_1);
                            break;
                        case 1:
                            chapterName= getString(R.string.chapter_2);
                            break;
                        case 2:
                            chapterName= getString(R.string.chapter_3);
                            break;
                        case 3:
                            chapterName= getString(R.string.chapter_4);
                        default:
                            break;
                    }
                    ((ChapterActivity) getActivity()).setTitle(getString(R.string.chapter_screen_title) + ">" + chapterName);
                    AppData.currentChapterName = chapterName;
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragmentLayout, new ContentDetailFragment())
                            .commit();

                }
            }
        });

        return  view;
    }

    private void loadBookData(){
        String[] bookName = {
                getString(R.string.chapter_1),
                getString(R.string.chapter_2),
                getString(R.string.chapter_3),
                getString(R.string.chapter_4),
        };
        if (mListBook==null)
            mListBook= new ArrayList<>();
        for (int i=0;i<4;i++){
            ChapterFragment.LessonLevel1 lessonLevel1 = new ChapterFragment.LessonLevel1();
            lessonLevel1.bookName = bookName[i];
            if (i<4)
                lessonLevel1.hasContent = true;
            else
                lessonLevel1.hasContent = false;
            mListBook.add(lessonLevel1);
        }
    }
    class LessonAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mListBook.size();
        }

        @Override
        public Object getItem(int position) {
            return mListBook.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
            {
                convertView = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.item_lesson_level1,parent,false);
            }
            ImageView nextButton = (ImageView)convertView.findViewById(R.id.next_button);
            TextView bookName = (TextView)convertView.findViewById(R.id.bookName);
            if (!mListBook.get(position).hasContent)
                nextButton.setVisibility(View.GONE);
            bookName.setText(mListBook.get(position).bookName);
            return convertView;
        }
    }
    class LessonLevel1 {
        String bookThumb;
        String bookName;
        Boolean hasContent;
    }
}
