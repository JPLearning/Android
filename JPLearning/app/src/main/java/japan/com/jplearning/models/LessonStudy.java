package japan.com.jplearning.models;

/**
 * Created by HK on 1/12/2017.
 */

public class LessonStudy {
    private int type_id ; //1 : nghe , m2 nhìn , 3 đi
    private int lesson_id;
    private String startDate;
    private String endDate;
    private int status_id; //0 chưa học , 1 đã học, m2 kệ cha mày
    private String access_token;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    private String word;
    public int getType() {
        return type_id;
    }

    public void setType(int type) {
        this.type_id = type;
    }

    public int getLesson_id() {
        return lesson_id;
    }

    public void setLesson_id(int lesson_id) {
        this.lesson_id = lesson_id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status_id;
    }

    public void setStatus(int status) {
        this.status_id = status;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
