package japan.com.jplearning.utils;



/*
 * Created by Dell on 6/8/2017.
 */

import japan.com.jplearning.interfaces.JPLearningAPI;

public class APIUtils {

    public static final String BASE_URL = "http://18.217.212.16/backend_jp/api/v1/";
//    public static final String BASE_URL = "http://ec2-54-89-34-55.compute-1.amazonaws.com:10200/api/";
//    public static final String BASE_URL = "http://192.168.0.108:10200/api/";

    public static JPLearningAPI getMarcoService() {
        return RetrofitClient.getClient(BASE_URL).create(JPLearningAPI.class);
    }

    public static String getImageUrl(String container,String url){
        if (url.startsWith("http"))
            return url;
        else
            return String.format("%sstorages/%s/download/%s",BASE_URL,container,url);
    }

    //get more service for chat

}
