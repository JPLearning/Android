package japan.com.jplearning;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.speech.SpeechRecognizer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import japan.com.jplearning.fragment.ConversationFragment;
import japan.com.jplearning.fragment.LessonFragment;
import japan.com.jplearning.fragment.OnChangeFragment;
import japan.com.jplearning.fragment.RecordFragment;
import japan.com.jplearning.fragment.ResultLearningFragment;
import japan.com.jplearning.fragment.VocubularyFragment;
import japan.com.jplearning.fragment.WordLearnFragment;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.views.ActionItem;
import japan.com.jplearning.views.QuickAction;

public class MainActivity extends AppCompatActivity {

    Button bt1,bt2,bt3,bt4,btnToast;
    int ID_ADD = 1,ID_ACCEPT = 2,ID_UPLOAD = 3;
    QuickAction mQuickAction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        TextView videoTitle = findViewById(R.id.video_title);
//        videoTitle.setText(AppData.videoName);
        btnToast = (Button) findViewById(R.id.btnTitle);
        btnToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             final  Toast toast = Toast.makeText(MainActivity.this, Html.fromHtml("<font color='#ffffff' size='50sp' ><b>"+
                        AppData.currentLessonName+ "</b></font>"),Toast.LENGTH_LONG);
                 toast.setGravity(Gravity.TOP|Gravity.LEFT, 300, 170);
                toast.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 1000);
               // Toast.makeText(MainActivity.this, AppData.currentLessonName, Toast.LENGTH_LONG).show();
            }
        });

        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setBackgroundResource(R.drawable.button_main_selected);
        bt2 = (Button)findViewById(R.id.bt2);
        bt3 = (Button)findViewById(R.id.bt3);
        bt4 = (Button)findViewById(R.id.bt4);

        bt1.setOnClickListener(onClickListener);
        bt2.setOnClickListener(onClickListener);
        bt3.setOnClickListener(onClickListener);
        bt4.setOnClickListener(onClickListener);

        findViewById(R.id.viewThumbNail).setOnClickListener(onClickListener);
        findViewById(R.id.back).setOnClickListener(onClickListener);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragmentLayout, new LessonFragment());
        ft.commit();

        ActionItem addItem 		= new ActionItem(ID_ADD, "かばんをかいたい",getThumnail(1000));
        ActionItem acceptItem 	= new ActionItem(ID_ACCEPT, "デバートでうりばをききました", getThumnail(20000));
        ActionItem uploadItem 	= new ActionItem(ID_UPLOAD, "ほんをかいたい", getThumnail(50000));

        //use setSticky(true) to disable QuickAction dialog being dismissed after an item is clicked
        uploadItem.setSticky(true);

        mQuickAction 	= new QuickAction(this);

        mQuickAction.addActionItem(addItem);
        mQuickAction.addActionItem(acceptItem);
        mQuickAction.addActionItem(uploadItem);

        //setup the action item click listener
        mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);

                if (actionId == ID_ADD) {
                    //Toast.makeText(getApplicationContext(), "Add item selected", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getApplicationContext(), actionItem.getTitle() + " selected", Toast.LENGTH_SHORT).show();
                }
                mQuickAction.dismiss();
            }
        });

        mQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
            @Override
            public void onDismiss() {
               // Toast.makeText(getApplicationContext(), "Ups..dismissed", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private Drawable getThumnail(long time){
        Uri  videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                +R.raw.video_0_1);
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, videoURI);
        Bitmap bitmap = retriever
                .getFrameAtTime(time, MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 350, 200, true));
//        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
        return d;
    }
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            bt1.setBackgroundResource(R.drawable.button_main);
            bt2.setBackgroundResource(R.drawable.button_main);
            bt3.setBackgroundResource(R.drawable.button_main);
            bt4.setBackgroundResource(R.drawable.button_main);
            switch (v.getId()){
                case R.id.bt1:
                    bt1.setBackgroundResource(R.drawable.button_main_selected);
                    ft.replace(R.id.fragmentLayout, new LessonFragment());
                    ft.commit();
                    break;
                case R.id.bt2:
                    bt2.setBackgroundResource(R.drawable.button_main_selected);
                    ft.replace(R.id.fragmentLayout, new OnChangeFragment());
                    ft.commit();
                    break;
                case R.id.bt3:
                    if (Build.VERSION.SDK_INT >= 23) {
                        // Pain in A$$ Marshmallow+ Permission APIs
                        requestForPermission();
                    }
                    bt3.setBackgroundResource(R.drawable.button_main_selected);

                    ft.replace(R.id.fragmentLayout, new ConversationFragment());
                    ft.commit();
                    break;
                case R.id.bt4:
                    bt4.setBackgroundResource(R.drawable.button_main_selected);
                    ft.replace(R.id.fragmentLayout, new ResultLearningFragment());
                    ft.commit();
                    break;
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.viewThumbNail:
                    mQuickAction.show(v);
                    mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_CENTER);
                    break;
            }
        }
    };

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    @TargetApi(Build.VERSION_CODES.M)
    private void requestForPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!isPermissionGranted(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("Require for Speech to text");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {

                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);

                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }

                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        //add listeners on view
        //setUpView();
    }
    @TargetApi(Build.VERSION_CODES.M)
    private boolean isPermissionGranted(List<String> permissionsList, String permission) {

        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);

            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                        ) {
                    // All Permissions Granted

                    //add listeners on view
                    //setUpView();

                } else {
                    // Permission Denied
                    Toast.makeText(MainActivity.this, "Some Permissions are Denied Exiting App", Toast.LENGTH_SHORT)
                            .show();

                    finish();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
