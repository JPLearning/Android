package japan.com.jplearning.helper;

import android.text.TextUtils;



import java.io.File;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by HienNguyen on 12/20/16.
 */

public class StringUtils {

    public static final String EMPTY = "";


    public static String endcodeMD5(String code) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(code.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean checkParams(String... agr){
        for (String s : agr){
            if(s.isEmpty()){
                return false;
            }
        }
        return true;
    }



    public static String duration2String(long time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(time));
        double minute = calendar.get(Calendar.MINUTE);
        double second = calendar.get(Calendar.SECOND);

        DecimalFormat format = new DecimalFormat("00");
        return format.format(minute) + ":" + format.format(second);
    }



    public static List<String> convertWordList(String wordList) {
        return Arrays.asList(wordList.split(" "));
    }

    public static List<Integer> convertPeriodList(String periodList) {
        String[] periodString = periodList.split(",");
        List<Integer> result = new ArrayList<>();
        for (String period : periodString) {
            result.add(Integer.valueOf(period));
        }
        return result;
    }





}
