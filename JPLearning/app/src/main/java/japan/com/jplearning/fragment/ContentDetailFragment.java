package japan.com.jplearning.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import japan.com.jplearning.ChapterActivity;
import japan.com.jplearning.LessonDetailActivity;
import japan.com.jplearning.PlayMusicActivity;
import japan.com.jplearning.PlayVideoActivity;
import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;

/**
 * Created by mac on 10/20/17.
 */

public class ContentDetailFragment extends Fragment {

    ListView mListView;
    List<ContentDetailFragment.LessonLevel1> mListBook;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_information_item,
                container,  false);
        mListView = (ListView) view.findViewById(R.id.listMainInfor);
        loadBookData();
        mListView.setAdapter(new ContentDetailFragment.LessonAdapter());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListBook.get(position).hasContent) {
                    if (AppData.currentChapter==0 )
                    {
                        Intent nextClass = new Intent(getActivity(), PlayVideoActivity.class);
                        startActivity(nextClass);
                    }
                    else  if( AppData.currentChapter==3)
                    {
                        AppData.currentLesson = position;
                        AppData.currentLessonName = mListBook.get(position).bookName;
                        Intent nextClass = new Intent(getActivity(), PlayMusicActivity.class);
                        startActivity(nextClass);
                    }
                    else {
                        AppData.currentLesson = position;
                        AppData.currentLessonName = mListBook.get(position).bookName;
                        Intent nextClass = new Intent(getActivity(), LessonDetailActivity.class);
                        startActivity(nextClass);
                    }
                }
        }
        });

        return view;
    }


    private void loadBookData() {
        String[][] bookName = {{
                "1章　日本語の基礎発音",

        }, {
                "1章　これ、ください",
                "2章　くつうりばはどこですか",
                "３章　くろいの、ありますか"
        },  {
                 "1章　自己紹介（じこしょうかい）",
                "2章　車いす移動（くるまいす移動）"
        }, {
                "1章 さくら（桜）",
                "2章 チューリップ",
                "3章 赤とんぼ"
        }};
        if (mListBook == null)
            mListBook = new ArrayList<>();
        int chapter_id = AppData.currentChapter;

        for (int i = 0; i < bookName[chapter_id].length; i++) {
            ContentDetailFragment.LessonLevel1 lessonLevel1 = new ContentDetailFragment.LessonLevel1();
            lessonLevel1.bookName = bookName[chapter_id][i];
            lessonLevel1.hasContent=true;
//            if (chapter_id==0)
//            {
//                lessonLevel1.hasContent=true;
//            }
//            else  if (chapter_id ==1) {
//                    lessonLevel1.hasContent = true;
//            }else
//                {
//                lessonLevel1.hasContent = true;
//            }
            mListBook.add(lessonLevel1);
        }
    }

    class LessonAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mListBook.size();
        }

        @Override
        public Object getItem(int position) {
            return mListBook.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.item_lesson_level2, parent, false);
            }

            ImageView nextButton = (ImageView) convertView.findViewById(R.id.next_button);
            TextView bookName = (TextView) convertView.findViewById(R.id.bookName);
            if (!mListBook.get(position).hasContent)
                nextButton.setVisibility(View.GONE);
            bookName.setText(mListBook.get(position).bookName);
            return convertView;
        }
    }

    class LessonLevel1 {
        String bookThumb;
        String bookName;
        Boolean hasContent;
    }
}
