package japan.com.jplearning;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.helper.Utilities;
import japan.com.jplearning.models.ClientUser;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HK on 10/10/2017.
 */

public class LoginActivity extends AppCompatActivity {

    Button mLogin;
    EditText mLoginId,mLoginPassword;
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLogin = (Button) findViewById(R.id.login_button);
        mLoginId = (EditText)findViewById(R.id.login_id);
        mLoginPassword = (EditText)findViewById(R.id.password);
        progressBar = (ProgressBar)findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);
        mLogin.setEnabled(false);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                doLogin();
            }
        });
    }

    private boolean checkValid(){
        return false;
    }
    Timer timer;

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mLogin.getText().length()>0 && mLoginPassword.getText().length()>0){
                    runOnUiThread(new TimerTask() {
                        @Override
                        public void run() {
                            mLogin.setBackgroundResource(R.drawable.button_login_active);
                            mLogin.setEnabled(true);
                        }
                    });
                }else {
                    runOnUiThread(new TimerTask() {
                        @Override
                        public void run() {
                            mLogin.setBackgroundResource(R.drawable.button_login);
                            mLogin.setEnabled(true);
                        }
                    });
                }
            }
        },100,100);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }

    private  void doLogin(){
        //TODO call API
        ClientUser user = new ClientUser();
        user.setEmail(mLoginId.getText().toString());
        user.setPassword(mLoginPassword.getText().toString());
        APIUtils.getMarcoService().doLogin(user).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code()==200){
                    //OK
                    progressBar.setVisibility(View.GONE);
                    try {
                        if (response.body().get("error").getAsInt() == 0) {
                            AppData.mUser = ClientUser.createUserFromJsonData(response.body().getAsJsonObject("data"));
                            startActivity(new Intent(LoginActivity.this, ChapterActivity.class));
                            finish();
                            return;
                        }
                    }catch(Exception ex){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                        builder1.setMessage("invalid email or password ");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                builder1.setMessage("Network error.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

            }
        });
    }
}
