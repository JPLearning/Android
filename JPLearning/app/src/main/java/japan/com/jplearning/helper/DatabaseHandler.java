package japan.com.jplearning.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import japan.com.jplearning.models.HLesson;
import japan.com.jplearning.models.HPhrasing;
import japan.com.jplearning.models.HTesting;
import japan.com.jplearning.models.HWording;
import japan.com.jplearning.models.Lesson;

/**
 * Created by HK on 22/1/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 7;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_HWording = "words";
    private static final String TABLE_HPhrasing = "phrases";
    private static final String TABLE_HLesson = "lessons";
    private static final String TABLE_HTesting = "tests";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME_WORDING = "word";
    private static final String KEY_NAME_PHRASING = "phrase";
    private static final String KEY_NAME_LESSON = "title";
    private static final String KEY_START_DATE = "start_date";
    private static final String KEY_END_DATE = "end_date";
    private static final String KEY_NAME_RESULT = "result";
    private static final String KEY_IS_LISTEN = "listen";
    private static final String KEY_IS_SPEAK = "speak";
    private static final String KEY_IS_LEARN = "learn";
    private static final String KEY_NUMBER = "number_exc";
    private static final String KEY_TYPE = "type";//0 word , 1 phase
    private static final String KEY_STATUS = "status";
    private static final String KEY_NAME_QUESTION = "question";
    private static final String KEY_COMMENT = "comment";
    private static final String KEY_LESSON_ID = "lesson_id"; //2,5,8
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HWording_TABLE = "CREATE TABLE " + TABLE_HWording + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_WORDING + " TEXT,"
                + KEY_NUMBER + " INTEGER," + KEY_TYPE + " INTEGER,"
                + KEY_START_DATE + " TEXT," + KEY_END_DATE + " TEXT," +KEY_COMMENT+ " TEXT ," +KEY_LESSON_ID+ " TEXT "+")";

        String CREATE_HPhrasing_TABLE = "CREATE TABLE " + TABLE_HPhrasing + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_PHRASING + " TEXT,"
                + KEY_START_DATE + " TEXT," +KEY_COMMENT+ " TEXT "+")";

        String CREATE_HLesson_TABLE = "CREATE TABLE " + TABLE_HLesson + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_LESSON + " TEXT,"
                + KEY_IS_LISTEN + " INTEGER DEFAULT 0," + KEY_IS_SPEAK + " INTEGER DEFAULT 0," + KEY_IS_LEARN + " INTEGER DEFAULT 0,"
                + KEY_START_DATE + " TEXT," + KEY_END_DATE + " TEXT," +KEY_COMMENT+ " TEXT ," +KEY_LESSON_ID+ " TEXT "+")";

        String CREATE_HTesting_TABLE = "CREATE TABLE " + TABLE_HTesting + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME_QUESTION + " TEXT," + KEY_STATUS + " INTEGER,"
                + KEY_START_DATE + " TEXT," + KEY_END_DATE + " TEXT," +KEY_COMMENT+ " TEXT ," +KEY_LESSON_ID+ " TEXT "+")";

        db.execSQL(CREATE_HWording_TABLE);
        db.execSQL(CREATE_HPhrasing_TABLE);
        db.execSQL(CREATE_HLesson_TABLE);
        db.execSQL(CREATE_HTesting_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HWording);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HPhrasing);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HLesson);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HTesting);
        // Create tables again
        onCreate(db);
    }


    public void addWordHistory(HWording wording,int type,int lesson_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        HWording wId = getWording(wording.getWord());
        if (wId==null){
            values.put(KEY_NAME_WORDING, wording.getWord()); // Contact Name
            values.put(KEY_NUMBER,wording.getPractice()); // Contact Phone
            values.put(KEY_START_DATE,new Date().getTime());
            values.put(KEY_END_DATE,new Date().getTime());
            values.put(KEY_COMMENT,wording.getComment());
            values.put(KEY_TYPE,type);
            values.put(KEY_LESSON_ID,lesson_id);
            db.insert(TABLE_HWording, null, values);

            // Closing database connection
        }else {
            values.put(KEY_NAME_WORDING, wording.getWord());
            values.put(KEY_END_DATE,new Date().getTime());
            values.put(KEY_TYPE,type);
            values.put(KEY_NUMBER, wording.getPractice());
            values.put(KEY_COMMENT,wording.getComment());
            db.update(TABLE_HWording, values, KEY_ID + " = ? ",
                    new String[] { String.valueOf(wId.getId()) });
        }
        db.close();
    }

    // Adding new lesson
    public void addHistoryLesson(HLesson lesson, int lesson_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        HLesson lesson1 = getLessonHistory(lesson.getTitle());
        if (lesson1==null){
            values.put(KEY_NAME_LESSON, lesson.getTitle()); // Contact Name
            values.put(KEY_IS_LISTEN, lesson.getListen()); // Contact Phone
            values.put(KEY_IS_LEARN, lesson.getLearn()); // Contact Name
            values.put(KEY_IS_SPEAK, lesson.getSpeak()); // Contact Phone
            values.put(KEY_START_DATE,new Date().getTime());
            values.put(KEY_END_DATE,new Date().getTime());
            values.put(KEY_COMMENT,lesson.getComment());
            values.put(KEY_LESSON_ID,lesson_id);
            db.insert(TABLE_HLesson, null, values);
             // Closing database connection
        }else {
            values.put(KEY_NAME_LESSON, lesson.getTitle()); // Contact Name
            if (lesson.getListen()>0)
                values.put(KEY_IS_LISTEN, lesson.getListen()); // Contact Phone
            if (lesson.getLearn()>0)
                values.put(KEY_IS_LEARN, lesson.getLearn()); // Contact Name
            if (lesson.getSpeak()>0)
                values.put(KEY_IS_SPEAK, lesson.getSpeak()); // Contact Phone

            values.put(KEY_END_DATE,new Date().getTime());// Closing database connection
            values.put(KEY_COMMENT,lesson.getComment());
            db.update(TABLE_HLesson, values, KEY_ID + " = ? ",
                new String[] { String.valueOf(lesson1.getId()) });
        }
        db.close();
    }
    // Add Test result
    public void addHistoryTest(HTesting test, int lesson_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        HTesting test1 = getTestHistory(test.getQuestion());
        int currentLesson=lesson_id;
        if(test1==null)
        {
            values.put(KEY_NAME_QUESTION, test.getQuestion());
            values.put(KEY_STATUS, test.getStatus());
            values.put(KEY_START_DATE,new Date().getTime());
            values.put(KEY_END_DATE,new Date().getTime());
           values.put(KEY_COMMENT,test.getComment());
            values.put(KEY_LESSON_ID,lesson_id);
            db.insert(TABLE_HTesting, null, values);
        }
        else
        {
//            if (test.getStatus()>0 )if(test1!=null && (KEY_LESSON_ID.equals(lesson_id))
//            values.put(KEY_STATUS,test.getStatus());
//            values.put(KEY_NAME_QUESTION, test.getQuestion());
            values.put(KEY_END_DATE,new Date().getTime());
            values.put(KEY_COMMENT,test.getComment());
           // values.put(KEY_LESSON_ID,lesson_id);
            db.update(TABLE_HTesting, values, KEY_ID + " = ? ",
                    new String[] { String.valueOf(test1.getId()) });
        }

        db.close();
    }

    HWording getWording (String wording) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_HWording + " where TRIM(word) = '" + wording.trim() + "'", null);
        HWording oWording = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                oWording = new HWording();
                oWording.setId(cursor.getInt(0));
                oWording.setComment(cursor.getString(6));
                oWording.setPractice(cursor.getColumnIndex(KEY_NUMBER));
                //cursor.close();
            }
           }
        return oWording;
    }
    // Getting single contact
    HLesson getLessonHistory(String lesson) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+TABLE_HLesson + " where "+ KEY_NAME_LESSON +"= '"+ lesson +"'",null);
//        Cursor cursor = db.query(TABLE_HLesson, new String[] { KEY_ID,
//                        KEY_NAME_LESSON, KEY_IS_SPEAK,KEY_IS_LEARN, KEY_IS_LISTEN, KEY_START_DATE }, KEY_NAME_LESSON + "='?'",
//                new String[] { lesson }, null, null, null, null);
        HLesson history = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
                try {
                    history = new HLesson(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4),
                            new Date(cursor.getLong(5)),null,cursor.getString(7));
                } catch (Exception ex) {
                    Log.e("DB error", ex.getMessage());
                    //cursor.close();
                }
                cursor.close();
                return history;
            }
        }

        return history;
    }
    //Get test
    HTesting getTestHistory (String question) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+TABLE_HTesting + " where "+ KEY_NAME_QUESTION +"= '"+ question +"'",null);
        HTesting oTesting = null;
        if (cursor != null) {
            while (cursor.moveToNext()) {
                DateFormat df = new SimpleDateFormat("dd/mm/yyyy");
                try {
                    oTesting = new HTesting(Integer.parseInt(cursor.getString(0)),
                            cursor.getString(1), cursor.getInt(2),
                            new Date(cursor.getLong(3)), null,cursor.getString(5));
                } catch (Exception ex) {
                    Log.e("DB error", ex.getMessage());
                    //cursor.close();
                }
                cursor.close();
                return oTesting;
            }
        }

        return oTesting;
    }

    // Getting All Contacts
    public List<HLesson> getAllLessonHistory(int lesson_id) {
        List<HLesson> historyList = new ArrayList<HLesson>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HLesson + " where lesson_id=" +lesson_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HLesson contact = new HLesson();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setTitle(cursor.getString(1));
                contact.setListen(cursor.getInt(2));
                contact.setLearn(cursor.getInt(4));
                contact.setSpeak(cursor.getInt(3));
                contact.setStartDate(new Date(cursor.getLong(5)));
                contact.setEndDate(new Date(cursor.getLong(6)));
                contact.setComment(cursor.getString(7));
                // Adding contact to list
                historyList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return historyList;
    }

    public List<HWording> getAllWordingHistory(int type, int lesson_id) {
        List<HWording> historyList = new ArrayList<HWording>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HWording + " where type=" + type + " and lesson_id=" + lesson_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HWording contact = new HWording();
                contact.setId(Integer.parseInt(cursor.getString(0)));
                contact.setWord(cursor.getString(1));
                contact.setPractice(cursor.getInt(2));
                contact.setStartDate(new Date(cursor.getLong(4)));
                contact.setEndDate(new Date(cursor.getLong(5)));
                contact.setComment(cursor.getString(6));
                // Adding contact to list
                historyList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return historyList;
    }

    //get all question
    public List<HTesting> getAllTestHistory(int lesson_id) {
        List<HTesting> historyList = new ArrayList<HTesting>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_HTesting + " where lesson_id=" +lesson_id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HTesting testq = new HTesting();
                testq.setId(Integer.parseInt(cursor.getString(0)));
                testq.setQuestion(cursor.getString(1));
                testq.setStatus(cursor.getInt(2));
                testq.setStartDate(new Date(cursor.getLong(3)));
                testq.setEndDate(new Date(cursor.getLong(4)));
                testq.setComment(cursor.getString(5));
                // Adding contact to list
                historyList.add(testq);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return historyList;
    }

}