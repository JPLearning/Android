package japan.com.jplearning.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import japan.com.jplearning.R;
import japan.com.jplearning.adapters.WordAdapter;
import japan.com.jplearning.customview.CustomVideoView;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.interfaces.UpdateUI;
import japan.com.jplearning.models.LessonStudy;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by HK on 18/10/2017.
 */

public class WordLearnFragment extends Fragment implements UpdateUI {

    private LinearLayout mSubLayout;
    CustomVideoView videoView;
    SeekBar mSeekBar;
    int position = 0;
    List<Subtitle> mListSub;
    Button playWordByVideo;

    String[][] mListWords = {
            {"", "", "世話", "", "", "", "", "", "", "", "", "", "", "", ""},
            {"", "号室", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
            {"", "", "くつ", "かい", "", "", "ビジネス", "あちら", "となり", "", "", ""}
    };

    List<TextInput> mListText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListSub = new ArrayList<>();
        loadSurfFile();
        processWords();
    }

    private static final int VERTICAL_ITEM_SPACE = 48;
    MyViewAdapter adapter;
    RecyclerView recyclerView;
    RecyclerView recyclerView0;
    RelativeLayout firstLayout;

    RelativeLayout crazyLayout;

    String[][] listwords = {{"世話"}, {"号室"}, {
            "くつ",
            "かい",
            "ビジネス",
            "あちら",
            "となり"
    }};
    int[][] times = {{0, 0, 11080, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 9130, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 5100, 9130, 0, 0, 16240, 21110, 24020, 0, 0, 0}};
    WordAdapter mWordAdapter;
    List<WordAdapter.DataBind> dataBinds;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_learning_word, container, false);
        TextView videoTitle = view.findViewById(R.id.video_title);
        videoTitle.setText(AppData.videoName);
        crazyLayout = view.findViewById(R.id.crazyLayout);

        recyclerView = (RecyclerView) view.findViewById(R.id.listwords);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //add ItemDecoration
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
        //or
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        //or
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.divider));


        adapter = new MyViewAdapter(mListText);
        recyclerView.setAdapter(adapter);

        recyclerView0 = (RecyclerView) view.findViewById(R.id.listWordToLearn);
        RecyclerView.LayoutManager mLayoutManager0 = new LinearLayoutManager(getActivity());
        recyclerView0.setLayoutManager(mLayoutManager0);
        recyclerView0.setItemAnimator(new DefaultItemAnimator());
        dataBinds = new ArrayList<>();

        int lessonId = -1;
        if (AppData.currentChapter == 0) {
            lessonId = AppData.currentLesson;
        } else {
            lessonId = 2;
        }

        for (int i = 0; i < listwords[lessonId].length; i++) {
            dataBinds.add(new WordAdapter.DataBind(listwords[lessonId][i]));
        }

        mWordAdapter = new WordAdapter(dataBinds);
        recyclerView0.setAdapter(mWordAdapter);
        recyclerView0.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        //or
        recyclerView0.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView0.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.divider));
        loadLeftContent(view);
        firstLayout = (RelativeLayout) view.findViewById(R.id.first_layout);
        playWordByVideo = view.findViewById(R.id.playWordByVideo);

        playWordByVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstLayout.setVisibility(View.GONE);
                videoView.setVisibility(View.VISIBLE);
                //videoView.start();
                updateSelectedWords();
                videoView.start();
                adapter = new MyViewAdapter(mListText);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                //recyclerView.setVisibility(View.VISIBLE);
                crazyLayout.setVisibility(View.VISIBLE);

            }
        });

        return view;
    }
    private LessonStudy lStudy;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DateFormat df = new DateFormat();
        lStudy = new LessonStudy();
        lStudy.setStartDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
        lStudy.setAccess_token(AppData.mUser.getAccess_token());
        lStudy.setLesson_id(AppData.currentLesson);
        lStudy.setType(2);

        APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void loadLeftContent(final View view) {

        mSubLayout = (LinearLayout) view.findViewById(R.id.has_sub_layout);

        videoView = (CustomVideoView) view.findViewById(R.id.video_view);
        mSeekBar = (SeekBar) view.findViewById(R.id.seekbarStatus);
        String path = null;
        if (AppData.currentChapter == 0) {
            if (AppData.currentLesson == 0)
                path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_0_0;
            else
                path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_0_1;
        } else if (AppData.currentChapter == 1) {
            path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_1_0;
        }

        videoView.setVideoURI(Uri.parse(path));

        final MediaController mc = new MediaController(getActivity());
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (position == 0) {
                    try {
                        videoView.requestFocus();
                        mSeekBar.setMax(videoView.getDuration());
                        mSeekBar.postDelayed(onEverySecond, 100);
                        updateStatus();

                        videoView.setMediaController(mc);

                        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                        lp.gravity = Gravity.BOTTOM;
                        mc.setLayoutParams(lp);

                        ((ViewGroup) mc.getParent()).removeView(mc);

                        ((FrameLayout) view.findViewById(R.id.videoControllerLayout)).addView(mc);
                        // mediaPlayer.start();
                        mc.show(0);
                        // readSubtitle(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.sublite));
                    } catch (Exception e) {
                        System.out.printf("Error playing video %s\n", e);
                    }
                } else {
                    videoView.pause();
                }

            }
        });


        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {
                mSeekBar.postDelayed(onEverySecond, 500);
            }

            @Override
            public void onPause() {
                Log.e("Pause Time", "" + videoView.getCurrentPosition());
                //mSeekBar.postDelayed(onEverySecond, 10);
                //adapter.notifyDataSetChanged();
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                mp.pause();
                mc.show(1);
            }
        });
        videoView.setVisibility(View.GONE);
        // videoView.start();
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if (fromUser) {
//                     this is when actually seekbar has been seeked to a new position
                    int time = videoView.getDuration();
                    if (videoView.isPlaying()) {
                        videoView.pause();
                        videoView.seekTo(progress);
                        videoView.start();
                    } else {
                        videoView.seekTo(progress);
                        videoView.pause();
                    }
//                    int totalDuration = videoView.getDuration();
//                    int currentPosition = Utilities.progressToTimer(seekBar.getProgress(), totalDuration);
//
//                    // forward or backward to certain seconds
//                    if (videoView.isPlaying()) {
//                        videoView.seekTo(currentPosition);
//                        videoView.start();
//                    }else
//                        videoView.seekTo(currentPosition);

//
                }
            }
        });

    }

    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {

            if (mSeekBar != null) {
                mSeekBar.setProgress(videoView.getCurrentPosition());
            }
            int currentTime = videoView.getCurrentPosition();

            if (videoView.isPlaying()) {
                adapter.notifyDataSetChanged();
                updateListView(currentTime);
                mSeekBar.postDelayed(onEverySecond, 100);
            }


        }
    };

    private void updateListView(int currentTime) {
        //boolean isScroll = false;
        int i = -1;
        // int pos = -1;
        for (Subtitle subtitle : mListSub) {
            i++;
            if (subtitle.startTime <= currentTime && (subtitle.startTime + subtitle.duration) >= currentTime) {
                if (subtitle.isInputDone)
                    subtitle.isReading = false;
                else {
                    //isScroll = true;
                    subtitle.isReading = true;
                }
                // pos = i;
                if (mListText.get(i).inputText != null && !mListText.get(i).inputText.equals(""))
                    adapter.notifyDataSetChanged();
            } else
                subtitle.isReading = false;
        }
    }

    private void updateStatus() {

        Resources r = getResources();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int widthsc = displayMetrics.widthPixels;

        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, r.getDisplayMetrics());
        widthsc = widthsc / 2 - (int) px;

        long pivious = 0;
        for (Subtitle subtitle : mListSub) {


            float width = widthsc * ((float) subtitle.duration / videoView.getDuration());

            View view = new View(getActivity());
            view.setBackgroundColor(Color.parseColor("#5cb85c"));

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            if (pivious > 0) {
                float marginWidth = widthsc * ((float) (subtitle.startTime - pivious) / videoView.getDuration());
                lp.setMargins((int) marginWidth, 0, 0, 0);
            }
            pivious = subtitle.startTime;

            mSubLayout.addView(view, lp);
        }
    }

    private void processWords() {
        if (mListSub == null || mListSub.size() == 0)
            return;
        mListText = new ArrayList<>();
        int pos = 0;
        int lessonId = -1;
        if (AppData.currentChapter == 0) {
            lessonId = AppData.currentLesson;
        } else {
            lessonId = 2;
        }
        for (Subtitle subtitle : mListSub) {
            TextInput textInput = new TextInput();
            if (mListWords[lessonId][pos].length() > 0) {
                textInput.inputText = mListWords[lessonId][pos];
                textInput.firstText = subtitle.text.substring(0, subtitle.text.indexOf(mListWords[lessonId][pos]));
                textInput.finalText = subtitle.text.substring(subtitle.text.indexOf(mListWords[lessonId][pos]) + mListWords[lessonId][pos].length(), subtitle.text.length());

            } else {
                textInput.inputText = null;
                textInput.firstText = subtitle.text;
                textInput.finalText = null;
            }
            textInput.time = times[lessonId][pos];
            mListText.add(textInput);

            pos++;
        }

    }

    private void updateSelectedWords() {

        List<String> list = mWordAdapter.getItemChecked();
        for (int j = 0; j < mListText.size(); j++) {
            TextInput textInput = mListText.get(j);
            if (textInput.inputText == null || textInput.inputText.equals(""))
                continue;
            if (textInput.inputText != null && !textInput.inputText.equals("")) {
                boolean hasItem = false;
                for (int i = 0; i < list.size(); i++) {
                    if (textInput.inputText.equals(list.get(i))) {
                        hasItem = true;
                        break;
                    }
                }
                if (!hasItem) {
                    textInput.firstText += textInput.inputText;
                    textInput.inputText = null;
                }
            }
        }

        for (int k = 0; k < mListText.size(); k++) {
            if (mListText.get(k).inputText != null && !mListText.get(k).inputText.equals("")) {
                if (k > 0)
                    videoView.seekTo((int) mListSub.get(k - 1).startTime);
                break;
            }
        }

    }

    private void loadSurfFile() {
        if (mListSub == null)
            mListSub = new ArrayList<>();
        Scanner s = null;
        if (AppData.currentChapter == 0) {
            if (AppData.currentLesson == 0)
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_0));
            else
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_1));
        } else if (AppData.currentChapter == 1) {
            s = new Scanner(getResources().openRawResource(R.raw.subtitle_1_0));
        }
        if (s == null)
            return;

        int number = 0;
        try {
            while (s.hasNextLine()) {
                number++;
                String word = s.nextLine();
                if (number <= 2)
                    continue;
                Subtitle subtitle = new Subtitle();
                String time = s.nextLine();
                if (time.equals(""))
                    continue;
                subtitle.time = time;
                subtitle.text = s.nextLine();
                String[] times = subtitle.time.split(" --> ");
                String startTime = times[0];
                String endTime = times[1];
                subtitle.startTime = dateParseRegExp(startTime);
                long endTimeL = dateParseRegExp(endTime);
                subtitle.duration = endTimeL - subtitle.startTime;
                s.nextLine();
                mListSub.add(subtitle);
            }
        } finally {
            s.close();
        }
    }

    private static Pattern pattern = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");

    public static long dateParseRegExp(String period) {
        Matcher matcher = pattern.matcher(period);
        if (matcher.matches()) {
            return Long.parseLong(matcher.group(1)) * 3600000L
                    + Long.parseLong(matcher.group(2)) * 60000
                    + Long.parseLong(matcher.group(3)) * 1000
                    + Long.parseLong(matcher.group(4));
        } else {
            throw new IllegalArgumentException("Invalid format " + period);
        }
    }

    @Override
    public void onUpdate() {
        //adapter.notifyDataSetChanged();
//        mSeekBar.postDelayed(onEverySecond, 200);
    }

    @Override
    public void updateText(int text) {

    }


    class Subtitle {
        String time;
        long startTime;
        long duration;
        String text;
        boolean isReading = false;
        boolean isInputDone = false;
        boolean isChecked = false;
    }
    private int progress = 2;
    class MyViewAdapter extends RecyclerView.Adapter<MyViewAdapter.MyViewHolder> {

        List<TextInput> mListText;
       // UpdateUI mInterface;

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView firstText, lastText;
            public EditText inputText;

            public MyViewHolder(View view) {
                super(view);
                firstText = (TextView) view.findViewById(R.id.first_text);
                lastText = (TextView) view.findViewById(R.id.final_text);
                inputText = (EditText) view.findViewById(R.id.input_text);
            }
        }

        public MyViewAdapter(List<TextInput> sList) {

            this.mListText = new ArrayList<>();
            int i=-1;
            for (TextInput textInput :sList){
                i++;
                if(textInput.inputText!=null && !textInput.inputText.equals("")){
                    textInput.lineNumber = i;
                    this.mListText.add(textInput);
                }
            }
           // this.mInterface = mInterface;
        }

        @Override
        public MyViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_word_input, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewAdapter.MyViewHolder holder, final int position) {

            final TextInput words = mListText.get(position);
            //focus text here
            if (words.firstText != null) {
                holder.firstText.setText(words.firstText);
                holder.firstText.setVisibility(View.VISIBLE);
            } else
                holder.firstText.setText("");
            if (words.finalText != null) {
                holder.lastText.setText(words.finalText);
                holder.lastText.setVisibility(View.VISIBLE);
            } else {
                holder.lastText.setVisibility(View.GONE);
            }

            if (words.inputText != null && !words.inputText.equals("")) {
                holder.inputText.setVisibility(View.VISIBLE);
                holder.inputText.setText("");
                if (mListSub.get(words.lineNumber).isReading) {
                    holder.inputText.setHintTextColor(Color.parseColor("#5cb85c"));
                    setHint(words.inputText.length(), holder.inputText);
                    if (videoView.getCurrentPosition() > words.time) {
                        if (videoView.isPlaying()) {
                            videoView.pause();
                        }
                    }
                    // (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    holder.inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                            if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                                mListText.get(position).inputedText = holder.inputText.getText().toString();
                                if (holder.inputText.getText().toString().equals(mListText.get(position).inputText)) {
                                    videoView.start();
                                    holder.inputText.setTextColor(Color.parseColor("#5cb85c"));
                                    lStudy.setWord(holder.inputText.getText().toString());
                                    lStudy.setStatus(1);
                                    DateFormat df = new DateFormat();
                                    lStudy.setEndDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
                                    APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
                                        @Override
                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {

                                        }
                                    });

                                } else {

                                    lStudy.setWord(mListText.get(position).inputText);
                                    lStudy.setStatus(progress);
                                    DateFormat df = new DateFormat();
                                    lStudy.setEndDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
                                    APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
                                        @Override
                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {

                                        }
                                    });

                                    if (videoView.isPlaying())
                                        videoView.pause();
                                    holder.inputText.setTextColor(Color.parseColor("#FF0000"));
                                }

                                mListSub.get(words.lineNumber).isInputDone = true;
                            }
                            return false;
                        }
                    });
                    holder.inputText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                holder.inputText.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(holder.inputText, InputMethodManager.SHOW_IMPLICIT);
                                    }
                                });
                            }
                        }
                    });

                    holder.inputText.requestFocus();
                } else {
                    holder.inputText.setHintTextColor(Color.parseColor("#d3d3d3"));
                    holder.inputText.setText("");
                    if (mListText.get(position).inputedText != null && !mListText.get(position).inputedText.equals(""))
                        holder.inputText.setText(mListText.get(position).inputedText);
                    else {
                        holder.inputText.setHint("");
                    }
                    //holder.inputText.setHint("□ □ □");
                    setHint(words.inputText.length(), holder.inputText);
                }

            } else
                holder.inputText.setVisibility(View.GONE);

        }

        @Override
        public int getItemCount() {
            return mListText.size();
        }
    }

    private void setHint(int wordNumber, EditText editText) {
        String word = "";
        for (int i = 0; i < wordNumber; i++) {
            word += "□";
        }
        editText.setHint(word);
    }

    class TextInput {
        String firstText;
        String inputText;
        String finalText;
        String inputedText;
        int time;
        int lineNumber;
    }

    class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable divider;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            divider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            divider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int verticalSpaceHeight;

        public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
            this.verticalSpaceHeight = verticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = verticalSpaceHeight;
        }
    }

    Timer timer;

    @Override
    public void onResume() {
        super.onResume();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(getContext()== null){
                    return;
                }
                for (int i = 0; i < dataBinds.size(); i++) {

                    if (dataBinds.get(i).isChecked) {
                        getActivity().runOnUiThread(new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity()==null)
                                    return;
                                playWordByVideo.setEnabled(true);
                                playWordByVideo.setBackground(getResources().getDrawable(R.drawable.button_select_word));
                            }
                        });
                        return;
                    }
                    getActivity().runOnUiThread(new TimerTask() {
                        @Override
                        public void run() {
                            if(getContext()== null)
                                return;
                            playWordByVideo.setEnabled(false);
                            playWordByVideo.setBackground(getResources().getDrawable(R.drawable.button_select_word_disable));
                        }
                    });
                }
            }
        }, 100, 100);
    }

    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
