package japan.com.jplearning;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import japan.com.jplearning.fragment.MusicFragment;
import japan.com.jplearning.helper.AppData;

public class PlayMusicActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);
        //Back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String title = String.format("%s> %s >%s", getString(R.string.chapter_screen_title), AppData.currentChapterName, AppData.currentLessonName);
        setTitle(title);
        // Call fragment
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragmentLayout, new MusicFragment());
        ft.commit();

    }
    public void setTitle(String text){
        TextView title = (TextView) findViewById(R.id.textTitle);
        title.setText(text);
    }
}