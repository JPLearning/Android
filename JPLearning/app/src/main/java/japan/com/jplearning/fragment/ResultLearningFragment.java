package japan.com.jplearning.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.helper.DatabaseHandler;
import japan.com.jplearning.models.HLesson;
import japan.com.jplearning.models.HTesting;
import japan.com.jplearning.models.HWording;

/*
 * Created by HK on 23/10/2017.
 */

public class ResultLearningFragment extends Fragment{


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    private static final String[] TABLE_HEADERS1 = { "Chapter1", "見る", "学ぶ", "話す" ,"開始日","終了日"};
    private static final String[][] DATA_TO_SHOW1 = { {"デパートでかい" +
            "ものする", "未・中・完", "未・中・完", "未・中・完" ,
             "", ""} };

    private static final String[] TABLE_HEADERS2 = { "単語", "単語", "学習状況（回数）", "クイズ開始" };
    private static final String[][] DATA_TO_SHOW2 = { {"□", "名前", "未・中・完", "①②〇〇"} };

    private static final String[] TABLE_HEADERS3 = { "文型", "単語", "学習状況（回数）","発音履歴書を残る"};
    private static final String[][] DATA_TO_SHOW3 = { {"□", "くつうりばどこですか", "①②〇〇"} };
    DatabaseHandler mDB;
    private static final int VERTICAL_ITEM_SPACE = 20;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mDB = new DatabaseHandler(getActivity());
        View view = inflater.inflate(R.layout.fragment_result_layout,
                container, false);

        final RecyclerView mRecyclerView1 = (RecyclerView)view.findViewById(R.id.list1);

        final RecyclerView mRecyclerView2 = (RecyclerView)view.findViewById(R.id.list2);
        final RecyclerView mRecyclerView3 = (RecyclerView)view.findViewById(R.id.list3);
        final RecyclerView mRecyclerView4 = (RecyclerView)view.findViewById(R.id.list4);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        LinearLayoutManager layoutManager3 = new LinearLayoutManager(getActivity());
        LinearLayoutManager layoutManager4 = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager2.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView1.setLayoutManager(layoutManager);

//        mRecyclerView1.addItemDecoration(
//                new DividerItemDecoration(getActivity(), R.drawable.line_divider));
        mRecyclerView1.setAdapter(new MyViewAdapter());
        mRecyclerView1.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        mRecyclerView2.setLayoutManager(layoutManager2);
        mRecyclerView2.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));
//        mRecyclerView2.addItemDecoration(
//                new DividerItemDecoration(getActivity(), R.drawable.line_divider));
        mRecyclerView2.setAdapter(new MyViewAdapter1());


        mRecyclerView3.setLayoutManager(layoutManager3);

//        mRecyclerView3.addItemDecoration(
//                new DividerItemDecoration(getActivity(), R.drawable.line_divider));
        mRecyclerView3.setAdapter(new MyViewAdapter2());
        mRecyclerView3.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));


        mRecyclerView4.setLayoutManager(layoutManager4);
//        mRecyclerView4.addItemDecoration(
//                new DividerItemDecoration(getActivity(), R.drawable.line_divider));
        mRecyclerView4.setAdapter(new MyViewAdapter3());
        mRecyclerView4.addItemDecoration(new VerticalSpaceItemDecoration(VERTICAL_ITEM_SPACE));

        final Button button1 = view.findViewById(R.id.button1);
        final Button button2 = view.findViewById(R.id.button2);
        final Button button3=view.findViewById(R.id.button3);
        final Button button4=view.findViewById(R.id.button4);

        button1.setBackgroundResource(R.drawable.button_main_selected);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setBackgroundResource(R.drawable.button_main_selected);
                button2.setBackgroundResource(R.drawable.button_main);
                button3.setBackgroundResource(R.drawable.button_main);
                button4.setBackgroundResource(R.drawable.button_main);
                mRecyclerView1.setVisibility(View.VISIBLE);
                mRecyclerView2.setVisibility(View.GONE);
                mRecyclerView3.setVisibility(View.GONE);
                mRecyclerView4.setVisibility(View.GONE);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView1.setVisibility(View.GONE);
                mRecyclerView2.setVisibility(View.VISIBLE);
                mRecyclerView3.setVisibility(View.GONE);
                mRecyclerView4.setVisibility(View.GONE);
                button1.setBackgroundResource(R.drawable.button_main);
                button2.setBackgroundResource(R.drawable.button_main_selected);
                button3.setBackgroundResource(R.drawable.button_main);
                button4.setBackgroundResource(R.drawable.button_main);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView1.setVisibility(View.GONE);
                mRecyclerView2.setVisibility(View.GONE);
                mRecyclerView3.setVisibility(View.VISIBLE);
                mRecyclerView4.setVisibility(View.GONE);
                button1.setBackgroundResource(R.drawable.button_main);
                button2.setBackgroundResource(R.drawable.button_main);
                button3.setBackgroundResource(R.drawable.button_main_selected);
                button4.setBackgroundResource(R.drawable.button_main);
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView1.setVisibility(View.GONE);
                mRecyclerView2.setVisibility(View.GONE);
                mRecyclerView3.setVisibility(View.GONE);
                mRecyclerView4.setVisibility(View.VISIBLE);
                button1.setBackgroundResource(R.drawable.button_main);
                button2.setBackgroundResource(R.drawable.button_main);
                button3.setBackgroundResource(R.drawable.button_main);
                button4.setBackgroundResource(R.drawable.button_main_selected);
            }
        });

        return view;

    }
    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int verticalSpaceHeight;

        public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
            this.verticalSpaceHeight = verticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            //outRect.bottom = verticalSpaceHeight;
            if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1
                    && parent.getChildAdapterPosition(view) != 0)
            {
                outRect.bottom = verticalSpaceHeight;
            }
        }
    }

    public class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private  final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable divider;
        /**
         * Default divider will be used
         */

        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            divider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
           // divider = context.getResources().getDrawable(R.drawable.line_divider);
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            divider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();

                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }

    }

    public class MyViewAdapter extends RecyclerView.Adapter<MyViewAdapter.ViewHolder>{

        public class ViewHolder extends RecyclerView.ViewHolder{

            //These are the general elements in the RecyclerView
            public TextView title;
            public TextView listenStatus;
            public TextView learnStatus;
            public TextView speakStatus;
            public TextView startDate, endDate, tv_comment;
            public EditText ed_comment;
            public Button  btn_ok,btn_cancel;
            public ImageView btn_cmt;
            public  RelativeLayout box_comment;



            //This constructor would switch what to findViewBy according to the type of viewType
            public ViewHolder(View v, int viewType) {
                super(v);
                if (viewType == 0) {

                } else if (viewType == 1) {
                    title = (TextView)v.findViewById(R.id.title);
                    listenStatus = (TextView)v.findViewById(R.id.listen);
                    learnStatus = (TextView)v.findViewById(R.id.learn);
                    speakStatus = (TextView)v.findViewById(R.id.speak);
                    startDate = (TextView)v.findViewById(R.id.startDate);
                    endDate = (TextView)v.findViewById(R.id.endDate);
                    tv_comment = (TextView)v.findViewById(R.id.tv_comment);
                    ed_comment = (EditText) v.findViewById(R.id.ed_comment);
                    btn_cmt=(ImageView) v.findViewById(R.id.btn_cmt);
                    box_comment=(RelativeLayout)v.findViewById(R.id.box_comment) ;
                    btn_ok=(Button) v.findViewById(R.id.btn_ok) ;
                    btn_cancel=(Button) v.findViewById(R.id.btn_cancel) ;
                }
            }
        }


        @Override
        public MyViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            MyViewAdapter.ViewHolder vh;
            // create a new view
            switch (viewType) {
                case 0: //This would be the header view in my Recycler
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_header, parent, false);
                    vh = new ViewHolder(v,viewType);
                    return  vh;
                default: //This would be the normal list with the pictures of the places in the world
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_row, parent, false);
                    vh = new ViewHolder(v, viewType);
                    return vh;
            }
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            if (position>0 ) {
                final List<HLesson> mList = mDB.getAllLessonHistory(AppData.currentLesson);
                final HLesson lesson = mList.get(position - 1);
                    holder.title.setText(lesson.getTitle());
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                if (lesson.getListen()>0)
                    holder.listenStatus.setText("●");
                else
                    holder.listenStatus.setText("");

                if (lesson.getLearn()>0)
                    holder.learnStatus.setText("●");
                else
                    holder.learnStatus.setText("");

                if (lesson.getSpeak()>0)
                    holder.speakStatus.setText("●");
                else
                    holder.speakStatus.setText("");

                holder.startDate.setText("・"+dateFormat.format(lesson.getStartDate()));
                holder.endDate.setText("・"+dateFormat.format(lesson.getEndDate()));
                holder.tv_comment.setText(lesson.getComment());
                holder.btn_cmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.VISIBLE);
                    }
                });
                holder.btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        lesson.setComment(holder.ed_comment.getText().toString());
                        mDB.addHistoryLesson(lesson,AppData.currentLesson);
                        holder.tv_comment.setText(lesson.getComment());
                        holder.box_comment.setVisibility(view.GONE);
                    }
                });
                holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.GONE);

                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return mDB.getAllLessonHistory(AppData.currentLesson).size() +1;
        }

        @Override
        public int getItemViewType(int position) {
            int viewType = 1;
            if (position==0)
               viewType=0;
            return viewType;
        }
    }


    public class MyViewAdapter1 extends RecyclerView.Adapter<MyViewAdapter1.ViewHolder>{
        List<HWording> mList;
        public MyViewAdapter1(){
            mList = mDB.getAllWordingHistory(0,AppData.currentLesson);
        }
        public class ViewHolder extends RecyclerView.ViewHolder{
            //These are the general elements in the RecyclerView
            public TextView word,times,startDate,endDate,tv_comment;
            public EditText ed_comment;
            public Button  btn_ok,btn_cancel;
            public ImageView btn_cmt;
            public  RelativeLayout box_comment;

            public ViewHolder(View v, int viewType) {
                super(v);
                if (viewType == 0) {

                } else if (viewType == 1) {
                    word = (TextView)v.findViewById(R.id.word);
                    times =(TextView) v.findViewById(R.id.times);
                    startDate = (TextView)v.findViewById(R.id.startDate);
                    endDate =(TextView) v.findViewById(R.id.endDate);
                    tv_comment = (TextView)v.findViewById(R.id.tv_comment);
                    ed_comment = (EditText) v.findViewById(R.id.ed_comment);
                    btn_cmt=(ImageView) v.findViewById(R.id.btn_cmt);
                    box_comment=(RelativeLayout)v.findViewById(R.id.box_comment) ;
                    btn_ok=(Button) v.findViewById(R.id.btn_ok) ;
                    btn_cancel=(Button) v.findViewById(R.id.btn_cancel) ;
                }
            }
        }

        @Override
        public MyViewAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            MyViewAdapter1.ViewHolder vh;
            // create a new view
            switch (viewType) {
                case 0: //This would be the header view in my Recycler
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_header1, parent, false);
                    vh = new ViewHolder(v,viewType);
                    return  vh;
                default: //This would be the normal list with the pictures of the places in the world
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_row1, parent, false);
                    vh = new ViewHolder(v, viewType);
                    return vh;
            }
        }

        @Override
        public void onBindViewHolder(final MyViewAdapter1.ViewHolder holder, int position) {
            if (position>0) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                final HWording wording = mList.get(position-1);
                holder.word.setText(wording.getWord());
                holder.times.setText(wording.getPractice() + "");
                holder.startDate.setText("・"+dateFormat.format(wording.getStartDate()));
                holder.endDate.setText("・"+dateFormat.format(wording.getEndDate()));
                holder.tv_comment.setText(wording.getComment());
                holder.btn_cmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.VISIBLE);
                    }
                });
                holder.btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        wording.setComment(holder.ed_comment.getText().toString());
                        mDB.addWordHistory(wording,0,AppData.currentLesson);
                        holder.tv_comment.setText(wording.getComment());
                        holder.box_comment.setVisibility(view.GONE);
                    }
                });
                holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.GONE);
                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return mList.size() +1;
        }

        @Override
        public int getItemViewType(int position) {
            int viewType = 1;
            if (position==0)
                viewType=0;
            return viewType;
        }
    }


    // Phrase
    public class MyViewAdapter2 extends RecyclerView.Adapter<MyViewAdapter2.ViewHolder>{
        public class ViewHolder extends RecyclerView.ViewHolder{
            //These are the general elements in the RecyclerView
            public TextView word,times,startDate,endDate,tv_comment;
            public EditText ed_comment;
            public Button btn_cancel, btn_ok;
            public ImageView btn_cmt;
            public  RelativeLayout box_comment;

            public ViewHolder(View v, int viewType) {
                super(v);
                if (viewType == 0) {
                } else if (viewType == 1) {
                    word = v.findViewById(R.id.word);
                    times = v.findViewById(R.id.times);
                    startDate = v.findViewById(R.id.startDate);
                    endDate = v.findViewById(R.id.endDate);
                    tv_comment = (TextView)v.findViewById(R.id.tv_comment);
                    ed_comment = (EditText) v.findViewById(R.id.ed_comment);
                    btn_cmt=(ImageView) v.findViewById(R.id.btn_cmt);
                    box_comment=(RelativeLayout)v.findViewById(R.id.box_comment) ;
                    btn_ok=(Button) v.findViewById(R.id.btn_ok) ;
                    btn_cancel=(Button) v.findViewById(R.id.btn_cancel) ;
                }
            }

        }

        @Override
        public MyViewAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            MyViewAdapter2.ViewHolder vh;
            // create a new view
            switch (viewType) {
                case 0: //This would be the header view in my Recycler
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_header2, parent, false);
                    vh = new ViewHolder(v,viewType);
                    return  vh;
                default: //This would be the normal list with the pictures of the places in the world
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_row2, parent, false);
                    vh = new ViewHolder(v, viewType);
                    return vh;
            }
        }

        @Override
        public void onBindViewHolder(final MyViewAdapter2.ViewHolder holder, int position) {
            if (position>0) {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                final HWording wording = mDB.getAllWordingHistory(1,AppData.currentLesson).get(position-1);
                holder.word.setText(wording.getWord());
                holder.times.setText(wording.getPractice() +"");
                holder.startDate.setText("・"+dateFormat.format(wording.getStartDate()));
                holder.endDate.setText("・"+dateFormat.format(wording.getEndDate()));
                holder.tv_comment.setText(wording.getComment());
                holder.btn_cmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.VISIBLE);
                    }
                });
                holder.btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        wording.setComment(holder.ed_comment.getText().toString());
                        mDB.addWordHistory(wording,1,AppData.currentLesson);
                        holder.tv_comment.setText(wording.getComment());
                        holder.box_comment.setVisibility(view.GONE);
                    }
                });
                holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.GONE);

                    }
                });


            }
        }

        @Override
        public int getItemCount() {
            return mDB.getAllWordingHistory(1,AppData.currentLesson).size()+1;
        }

        @Override
        public int getItemViewType(int position) {
            int viewType = 1;
            if (position==0)
                viewType=0;
            return viewType;
        }
    }


    // TEST RESULT
    public class MyViewAdapter3 extends RecyclerView.Adapter<MyViewAdapter3.ViewHolder>{
        List<HTesting> mList;
        public MyViewAdapter3(){
            mList = mDB.getAllTestHistory(AppData.currentLesson);
        }
        public class ViewHolder extends RecyclerView.ViewHolder{
            public TextView question,tv_comment;
            public TextView status;
            public TextView startDate, endDate;
            public EditText ed_comment;
            public ImageView btn_cmt;
            public Button btn_cancel, btn_ok;
            public  RelativeLayout box_comment;

            //This constructor would switch what to findViewBy according to the type of viewType
            public ViewHolder(View v, int viewType) {
                super(v);
                if (viewType == 0) {

                } else if (viewType == 1) {
                    question = (TextView)v.findViewById(R.id.question);
                    status = (TextView)v.findViewById(R.id.status);
                    startDate = (TextView)v.findViewById(R.id.startDate);
                    endDate = (TextView)v.findViewById(R.id.endDate);
                    tv_comment = (TextView)v.findViewById(R.id.tv_comment);
                    ed_comment = (EditText) v.findViewById(R.id.ed_comment);
                    btn_cmt=(ImageView) v.findViewById(R.id.btn_cmt);
                    box_comment=(RelativeLayout)v.findViewById(R.id.box_comment) ;
                    btn_ok=(Button) v.findViewById(R.id.btn_ok) ;
                    btn_cancel=(Button) v.findViewById(R.id.btn_cancel) ;
                }
            }
        }

        @Override
        public MyViewAdapter3.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v;
            MyViewAdapter3.ViewHolder vh;
            switch (viewType) {
                case 0: //This would be the header view in my Recycler
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_header3, parent, false);
                    vh = new ViewHolder(v,viewType);
                    return  vh;
                default: //This would be the normal list with the pictures of the places in the world
                    v = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_report_row3, parent, false);
                    vh = new ViewHolder(v, viewType);
                    return vh;
            }
        }

        @Override
        public void onBindViewHolder(final MyViewAdapter3.ViewHolder holder, int position) {
            if (position>0) {
                List<HTesting> mListTest = mDB.getAllTestHistory(AppData.currentLesson);
                final HTesting test = mListTest.get(position - 1);
                holder.question.setText(test.getQuestion().substring(0,4));
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
                if (test.getStatus()>0)
                    holder.status.setText("●");
                else
                    holder.status.setText("");
                holder.startDate.setText("・"+dateFormat.format(test.getStartDate()));
                holder.endDate.setText("・"+dateFormat.format(test.getEndDate()));
                holder.tv_comment.setText(test.getComment());
                holder.btn_cmt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.VISIBLE);
                    }
                });
                holder.btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        test.setComment(holder.ed_comment.getText().toString());
                        holder.tv_comment.setText(test.getComment());
                        mDB.addHistoryTest(test,AppData.currentLesson);
                        holder.box_comment.setVisibility(view.GONE);
                    }
                });
                holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        holder.box_comment.setVisibility(view.GONE);

                    }
                });
            }
        }
        @Override
        public int getItemCount() {
            return mDB.getAllTestHistory(AppData.currentLesson).size() +1;
        }
        @Override
        public int getItemViewType(int position) {
            int viewType = 1;
            if (position==0)
                viewType=0;
            return viewType;
        }
    }
}
