package japan.com.jplearning;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import japan.com.jplearning.fragment.LessonFragment;
import japan.com.jplearning.fragment.OnChangeFragment;
import japan.com.jplearning.helper.AppData;

/**
 * Created by HK on 19/10/2017.
 */

public class LessonDetailActivity extends AppCompatActivity{

    GridView mGridView;

    public String[][] mVideoTitles = {
            {"応用会話","じこしょうかい会話","専用単語の練習"},
            {"応用会話","車いす移動会話","専用単語の練習"},
            {"応用会話","ハンバーガーを かいます","専用単語の練習"},
            { "応用会話", "デバートでうりばをききました", "専用単語の練習"},
            {"応用会話", "かばんをかいたいです", "専用単語の練習"},
            {}
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_lesson_detail);
        mGridView = (GridView) findViewById(R.id.grid_thumnail);
        mGridView.setAdapter(new VideoThumb());
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==1 ) {
                    int convert = -1;
                    if (AppData.currentChapter == 2) {
                        if (AppData.currentLesson == 0) {
                            convert = AppData.currentLesson;
                            AppData.videoName = mVideoTitles[convert][position];
                            AppData.currentLesson = 2;
                        } else if (AppData.currentLesson == 1) {
                            convert = AppData.currentLesson;
                            AppData.videoName = mVideoTitles[convert][position];
                            AppData.currentLesson = 5;
                        }
                    }
                    else if (AppData.currentChapter == 1)
                    {
                        if (AppData.currentLesson == 0) {
                           convert =AppData.currentLesson;;
                            AppData.videoName = mVideoTitles[convert][position];
                            AppData.currentLesson = 1;
                        } else if (AppData.currentLesson == 1) {
                            convert = AppData.currentLesson;
                            AppData.videoName = mVideoTitles[convert][position];
                            AppData.currentLesson = 8;
                        }
                        else if (AppData.currentLesson == 2) {
                            convert = AppData.currentLesson;
                            AppData.videoName = mVideoTitles[convert][position];
                            AppData.currentLesson = 3;
                        }
                    }

                    startActivity(new Intent(LessonDetailActivity.this, MainActivity.class));
                }
                else
                    return;
            }
        });
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String title = String.format("%s> %s> %s",getString(R.string.chapter_screen_title),AppData.currentChapterName,AppData.currentLessonName);
        setTitle(title);
    }

    public void setTitle(String text){
        TextView title = (TextView) findViewById(R.id.textTitle);
        title.setText(text);
    }
    private Drawable getThumnail(long time){
        int chapterId = AppData.currentChapter;
        int lessonId = AppData.currentLesson;
        Uri  videoURI = null;
        if (chapterId==2){
            if (lessonId==0){
                videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                        +R.raw.video_0_0);
            }else {
                videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                        +R.raw.video_0_1);
            }
        }else  {
            if(lessonId==0)
            {
                videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                        +R.raw.video_1_1);
            }
            else if(lessonId==1){
                videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                        +R.raw.video_1_0);
            }
            else if(lessonId==2){
                videoURI = Uri.parse("android.resource://" + getPackageName() +"/"
                        +R.raw.video_1_2);
            }
        }

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(this, videoURI);
        Bitmap bitmap = retriever
                .getFrameAtTime(time, MediaMetadataRetriever.OPTION_PREVIOUS_SYNC);
        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
        return drawable;
    }
    class VideoThumb extends BaseAdapter {

        String[] mListVideoTitle;
        public VideoThumb(){
            //TODO load array string from asset
            int chapterId = AppData.currentChapter;
            int lessonId = AppData.currentLesson;
            int convert = -1;
            if (chapterId == 2){
                if (lessonId==0)
                    convert = 0;
                else if(lessonId==1)
                    convert = lessonId;
            }
            else if (chapterId == 1)
                convert = chapterId*2 + lessonId;
//            else if(chapterId==3){
//                convert=5;
//            }
            mListVideoTitle = mVideoTitles[convert];
        };
        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_lesson_detail,parent,false);

            RoundedImageView roundedImageView = (RoundedImageView)convertView.findViewById(R.id.thumbnail);
            ImageView crazyImage = convertView.findViewById(R.id.crazyImage);

            if (position==1) {
                roundedImageView.setBackground(getThumnail((position + 3) * 50000));
                roundedImageView.setImageDrawable(null);
            }
            else
                crazyImage.setVisibility(View.GONE);
            TextView video_title = (TextView)convertView.findViewById(R.id.video_title);
            video_title.setText(mListVideoTitle[position]);
            return convertView;
        }
    }
}
