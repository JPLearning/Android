package japan.com.jplearning.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.interfaces.UpdateUI;
import japan.com.jplearning.models.LessonStudy;
import japan.com.jplearning.translation_engine.TranslatorFactory;
import japan.com.jplearning.translation_engine.utils.ConversionCallaback;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/*
 * Created by HK on 17/10/2017.
 */

public class RecordFragment extends Fragment implements ConversionCallaback, UpdateUI {

    //List<Subtitle> mListSub;

    private final int REQ_CODE_SPEECH_INPUT = 100;
    List<String> listRecord;
//    String[] wordCheck = {
//            "くつ", "かい", "ビジネス", "あちら", "となり"
//    };
//    String[] wordVCheck = {
//            "Giày", "Tầng", "Business", "Chỗ đó", "Bên cạnh"
//    };

    class Sentence {
        String word_jp;
        String word_vi;
        String mp3;
        int type;
    }

    List<Sentence> mListData;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listRecord = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("lesson_id", AppData.currentLesson+"");
        APIUtils.getMarcoService().getListWordByLesson(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        if (jsonArray != null) {
                            Gson gson = new Gson();
                            String jsonOutput = jsonArray.toString();
                            Type listType = new TypeToken<List<Sentence>>(){}.getType();
                            mListData = (List<Sentence>) gson.fromJson(jsonOutput, listType);
                            if (mAdapter!=null)
                                mAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception ex) {

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        //loadSurfFile();
    }

    TextView recordText, CountDownText;
    FloatingActionButton recordImage;
    Button button1, btRight2, btRight1;
    static int count = 0;

    int progress = 2;
    TextView jpText, vnText;
    ImageView recordButton;
    ContentAdapter mAdapter;
    //MediaPlayer mp;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_record,
                container, false);
        final ListView listView = (ListView) view.findViewById(R.id.listwords);
        mAdapter = new ContentAdapter(this);
        listView.setAdapter(mAdapter);

//        TextView videoTitle = view.findViewById(R.id.record_lesson_name);
//        videoTitle.setText(AppData.videoName);
        jpText = view.findViewById(R.id.jp_text);
        vnText = view.findViewById(R.id.vn_text);
        recordButton = view.findViewById(R.id.record_button);
        CountDownText = view.findViewById(R.id.countDownText);
        button1 = view.findViewById(R.id.btTV);
        btRight2 = view.findViewById(R.id.btRight2);
        btRight1 = view.findViewById(R.id.btRight1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button1.setBackgroundResource(R.drawable.button_main_selected);
                if (vnText.getText().toString().isEmpty() && currentPosition > -1) {
                    vnText.setText(mListData.get(currentPosition).word_vi);
                } else {
                    button1.setBackgroundResource(R.drawable.button_main);
                    vnText.setText(null);
                }
            }
        });
        btRight2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
               // ft.replace(R.id.fragmentLayout, new RecordPhraseFragment());
                ft.commit();

            }
        });

        // mp = MediaPlayer.create(getActivity(), R.raw.sound_record);
        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // mp.start();
                recordButton.setImageResource(R.drawable.micro_red);
                count++;
                updateStatus();
            }
        });

        CountDownText.setText(count + "");
//        recordImage = (FloatingActionButton) view.findViewById(R.id.mic_record);
//        recordText = (TextView) view.findViewById(R.id.record_text);
//        recordLayout = (RelativeLayout) view.findViewById(R.id.record_layout);
//        recordLayout.setVisibility(View.GONE);
//        mainText = view.findViewById(R.id.main_text);
//        nextButton = view.findViewById(R.id.next_button);
//        nextButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                count +=1;
//                if (listRecord.size()-1 == count){
//                    nextButton.setVisibility(View.GONE);
//                    //count = 0;
//                }
//                mainText.setText(listRecord.get(count));
//            }
//        });
//        recordImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                recordImage.setImageResource(android.R.drawable.ic_media_pause);
//                promptSpeechInput();
//            }
//        });
//        startRecord = view.findViewById(R.id.record_button);
//        startRecord.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                recordLayout.setVisibility(View.VISIBLE);
//                listRecord.clear();
//                for (int i = 0; i < mListSub.size(); i++) {
//                    if (mListSub.get(i).isChecked) {
//                        listRecord.add(mListSub.get(i).text);
//                    }
//                }
//                if (listRecord.size() > 1)
//                    nextButton.setVisibility(View.VISIBLE);
//                else
//                    nextButton.setVisibility(View.GONE);
//
//                if (listRecord.size() > 0)
//                    mainText.setText(listRecord.get(0));
//                count = 0;
//
//                recordText.setText("");
//            }
//        });


        return view;
    }

    private void updateStatus() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recordButton.setImageResource(R.drawable.micro_black);
                CountDownText.setText(count + "");
                //  mp.start();
            }
        }, 2000);
    }

    LessonStudy lStudy = null;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DateFormat df = new DateFormat();

        lStudy = new LessonStudy();
        lStudy.setStartDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
        if (AppData.mUser != null)
            lStudy.setAccess_token(AppData.mUser.getAccess_token());
        lStudy.setLesson_id(AppData.currentLesson);
        lStudy.setType(3);
    }

    /**
     * Request Permission
     */
    private static final int TTS = 0;
    private static final int STT = 1;
    private static int CURRENT_MODE = -1;

    private void promptSpeechInput() {
//        Snackbar.make(view, "Listening", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()

        //Ask translator factory to start speech tpo text convertion
        //Hello There is optional
        TranslatorFactory.getInstance().
                getTranslator(TranslatorFactory.TRANSLATOR_TYPE.SPEECH_TO_TEXT, RecordFragment.this).
                initialize("Hello There", getActivity());

        CURRENT_MODE = STT;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                recordImage.setImageResource(android.R.drawable.ic_btn_speak_now);
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    recordText.setText(result.get(0));
                }
                break;
            }
        }
    }

    @Override
    public void onSuccess(String result) {
        //Toast.makeText(getActivity(), "Result " + result, Toast.LENGTH_SHORT).show();

        switch (CURRENT_MODE) {
            case STT:
                recordImage.setImageResource(android.R.drawable.ic_btn_speak_now);
                recordText.setText(result.split("\n")[0]);
                progress = 1;
                break;
        }
    }

    @Override
    public void onCompletion() {
        recordImage.setImageResource(android.R.drawable.ic_btn_speak_now);
    }

    @Override
    public void onErrorOccured(String errorMessage) {
        recordImage.setImageResource(android.R.drawable.ic_btn_speak_now);
        // Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        recordText.setText("");
    }

    @Override
    public void onUpdate() {

    }

    int currentPosition = -1;

    @Override
    public void updateText(int postion) {
        recordButton.setImageResource(R.drawable.micro_black);
        jpText.setText(mListData.get(postion).word_jp);
        vnText.setText(mListData.get(postion).word_vi);
        currentPosition = postion;
    }

    class ContentAdapter extends BaseAdapter {
        UpdateUI updateUI;

        public ContentAdapter(UpdateUI updateUI) {
            this.updateUI = updateUI;
        }

        @Override
        public int getCount() {
            return mListData!=null ? mListData.size():0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_list_word_check, parent, false);
            }
            final TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(mListData.get(position).word_jp);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count = 0;
                    CountDownText.setText(count + "");
                    updateUI.updateText(position);
                    vnText.setText(null);
                    button1.setBackgroundResource(R.drawable.button_main);
                }
            });


            return convertView;

        }

    }

//    private void loadSurfFile() {
//        Scanner s = null;
//        if (AppData.currentChapter==0) {
//            if (AppData.currentLesson==0)
//                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_0));
//            else
//                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_1));
//        }else if (AppData.currentChapter == 1){
//            s = new Scanner(getResources().openRawResource(R.raw.subtitle_1_0));
//        }
//        if (s==null)
//            return;
//        int number = 0;
//        try {
//            while (s.hasNextLine()) {
//                number++;
//                String word = s.nextLine();
//                if (number <= m2)
//                    continue;
//                Subtitle subtitle = new Subtitle();
//                String time = s.nextLine();
//                if (time.equals(""))
//                    continue;
//                subtitle.time = time;
//                subtitle.text = s.nextLine();
//                s.nextLine();
//                mListSub.add(subtitle);
//            }
//        } finally {
//            s.close();
//        }
//    }

    class Subtitle {
        String time;
        String text;
        boolean isChecked = false;
    }

    Timer timer;

    @Override


    public void onResume() {
        super.onResume();
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                if(getContext()== null){
//                    return;
//                }
//                for (int i = 0; i < mListSub.size(); i++) {
//
//                    if (mListSub.get(i).isChecked) {
//                        getActivity().runOnUiThread(new TimerTask() {
//                            @Override
//                            public void run() {
//                                if(getActivity()==null)
//                                    return;
//                                startRecord.setEnabled(true);
//                                startRecord.setBackground(getResources().getDrawable(R.drawable.button_select_word));
//                            }
//                        });
//                        return;
//                    }
//                    getActivity().runOnUiThread(new TimerTask() {
//                        @Override
//                        public void run() {
//                            if(getContext()== null)
//                                return;
//                            startRecord.setEnabled(false);
//                            startRecord.setBackground(getResources().getDrawable(R.drawable.button_select_word_disable));
//                        }
//                    });
//                }
//            }
//        }, 100, 100);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (timer != null)
            timer.cancel();
        count = 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        lStudy.setStatus(progress);
        DateFormat df = new DateFormat();
        lStudy.setEndDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
        APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
    }
}
