package japan.com.jplearning.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.interfaces.UpdateUI;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HK on 4/1/2018.
 */

public class PharseFragment extends Fragment implements UpdateUI {

    private SurfaceView preview=null;
    private SurfaceHolder previewHolder=null;
    private Camera camera=null;
    private boolean inPreview=false;
    private boolean cameraConfigured=false;

    List<String> listRecord;



    @Override
    public void onUpdate() {

    }

    int currentPosition = 0;
    static int count = 0;
    static  int tabNumber = 0;
    @Override
    public void updateText(int position) {
        voiceIcon.setImageResource(R.drawable.micro_black);
        jpText.setText(mListData.get(position).sentence_jp);
        vnText.setText(mListData.get(position).sentence_vi);
        currentPosition = position;
    }

    class Phrase {
        String sentence_jp;
        String sentence_vi;
        String mp3;
        int type;
        int id;
        int learn;
    }

    class Question {
        String question_text;
        List<String> options;
        int result;
    }
    List<Phrase> mListData;
    List<Question> mListQuestion;
    ContentAdapter mAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listRecord = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        map.put("lesson_id", AppData.currentLesson+"");
        APIUtils.getMarcoService().getListPhraseToLearn(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        if (jsonArray != null) {
                            Gson gson = new Gson();
                            String jsonOutput = jsonArray.toString();
                            Type listType = new TypeToken<List<Phrase>>(){}.getType();
                            mListData = (List<Phrase>) gson.fromJson(jsonOutput, listType);
                            if (mAdapter!=null)
                                mAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception ex) {

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        dummyData();
    }

    void dummyData(){
        Question q1 = new Question();
        q1.question_text = "今日からお世話になります";
        q1.options = new ArrayList<>();
        q1.options.add("明日からお世話になります");
        q1.options.add("今日からお世話になります");
        q1.options.add("昨日からお世話になります");

        Question q2 = new Question();
        q2.question_text = " これから施設の中を案内しましょう";
        q2.options = new ArrayList<>();
        q2.options.add("これから施設の中を説明しましょう");
        q2.options.add("これから施設の中を見せますしょう");
        q2.options.add("これから施設の中を案内しましょう");
        q2.options.add("これから施設の中を案内しましょう");

        Question q3 = new Question();
        q3.question_text = " これから施設の中を案内しましょう";
        q3.options = new ArrayList<>();
        q3.options.add("これから施設の中を説明しましょう");
        q3.options.add("これから施設の中を見せますしょう");
        q3.options.add("これから施設の中を案内しましょう");
        q3.options.add("これから施設の中を案内しましょう");

        Question q4 = new Question();
        q4.question_text = "これから施設の中を案内しましょう";
        q4.options = new ArrayList<>();
        q4.options.add("これから施設の中を説明しましょう");
        q4.options.add("これから施設の中を見せますしょう");
        q4.options.add("これから施設の中を案内しましょう");
        q4.options.add("これから施設の中を案内しましょう");


        Question q5 = new Question();
        q5.question_text = "これから施設の中を案内しましょう";
        q5.options = new ArrayList<>();
        q5.options.add("これから施設の中を説明しましょう");
        q5.options.add("これから施設の中を見せますしょう");
        q5.options.add("これから施設の中を案内しましょう");
        q5.options.add("これから施設の中を案内しましょう");
        mListQuestion = new ArrayList<>();
        mListQuestion.add(q1);
        mListQuestion.add(q2);
        mListQuestion.add(q3);
        mListQuestion.add(q4);
        mListQuestion.add(q5);
    }

    TextView textView,jpText,vnText,vn_text1,CountDownText;
    ImageView voiceIcon;
    Button bt1,bt2,bt3,btnVi;
    ListView lvQuestion;
    static int question_number = 0;
    RelativeLayout questionLayout,layout12,listWordRight;
    Button btAnswer;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_stupid_layout,
                container, false);
        preview=(SurfaceView)view.findViewById(R.id.webcam);
        previewHolder=preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        layout12 = view.findViewById(R.id.layout12);
        listWordRight=view.findViewById(R.id.listWordRight);
        questionLayout = view.findViewById(R.id.questionLayout);

        textView = view.findViewById(R.id.dkm_wc);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
                textView.setVisibility(View.GONE);
            }
        });

        voiceIcon = view.findViewById(R.id.image_voice);
        voiceIcon.setImageResource(R.drawable.micro_black);
        voiceIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voiceIcon.setImageResource(R.drawable.micro_red);
                new Handler().postDelayed(new TimerTask() {
                    @Override
                    public void run() {
                        count++;
                        voiceIcon.setImageResource(R.drawable.micro_black);
                        preview.setVisibility(View.GONE);
                        textView.setVisibility(View.VISIBLE);
                        CountDownText.setText(count + "");
                    }
                },3000);
            }
        });

        bt1 = view.findViewById(R.id.bt1);
        bt2 = view.findViewById(R.id.bt2);
        bt3 = view.findViewById(R.id.bt3);
        bt1.setBackgroundResource(R.drawable.button_main);
        bt2.setBackgroundResource(R.drawable.button_main_selected);
        layout12.setVisibility(View.VISIBLE);
        listWordRight.setVisibility(View.VISIBLE);
        questionLayout.setVisibility(View.GONE);
        btnVi = view.findViewById(R.id.btnVi);
        vn_text1 = view.findViewById(R.id.vn_text1);
        btnVi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnVi.setBackgroundResource(R.drawable.button_main_selected);
                if (vn_text1.getText().toString().isEmpty() && currentPosition > -1) {
                    vn_text1.setText(mListData.get(currentPosition).sentence_vi);
                } else {
                    btnVi.setBackgroundResource(R.drawable.button_main);
                    vn_text1.setText(null);
                }
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.button_main);
                bt2.setBackgroundResource(R.drawable.button_main_selected);
                bt3.setBackgroundResource(R.drawable.button_main);
                layout12.setVisibility(View.VISIBLE);
                listWordRight.setVisibility(View.VISIBLE);
                questionLayout.setVisibility(View.GONE);
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.button_main);
                bt2.setBackgroundResource(R.drawable.button_main);
                bt3.setBackgroundResource(R.drawable.button_main_selected);
                lvQuestion.setAdapter(new AnsAdapter(mListQuestion.get(question_number).options));
                layout12.setVisibility(View.GONE);
                listWordRight.setVisibility(View.GONE);
               questionLayout.setVisibility(View.VISIBLE);
                TextView text = view.findViewById(R.id.question_text);
                text.setText(mListQuestion.get(question_number).question_text);
                lvQuestion.setAdapter(new AnsAdapter(mListQuestion.get(question_number).options));
                btAnswer.setEnabled(false);
                btAnswer.setBackgroundResource(R.drawable.button_login_inactive);
            }
        });
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.button_main_selected);
                bt2.setBackgroundResource(R.drawable.button_main);
                bt3.setBackgroundResource(R.drawable.button_main);
                layout12.setVisibility(View.VISIBLE);
                listWordRight.setVisibility(View.VISIBLE);
                questionLayout.setVisibility(View.GONE);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragmentLayout, new VocubularyFragment());
                ft.commit();
            }
        });
        final ListView listView = (ListView) view.findViewById(R.id.listWord);
        mAdapter = new ContentAdapter(this);
        listView.setAdapter(mAdapter);

        jpText = view.findViewById(R.id.jp_text);
        vnText = view.findViewById(R.id.vn_text);
        CountDownText = view.findViewById(R.id.text_count_down);

        lvQuestion = view.findViewById(R.id.list_question);
        lvQuestion.setAdapter(new AnsAdapter(mListQuestion.get(question_number).options));
        btAnswer = view.findViewById(R.id.btn_anwser);
        btAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                question_number +=1;
                TextView text = view.findViewById(R.id.question_text);
                text.setText(mListQuestion.get(question_number).question_text);
                lvQuestion.setAdapter(new AnsAdapter(mListQuestion.get(question_number).options));
                if (question_number>=mListQuestion.size()-1)
                    question_number=0;
                btAnswer.setEnabled(false);
                btAnswer.setBackgroundResource(R.drawable.button_login_inactive);
            }
        });
        btAnswer.setEnabled(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        //openCamera();
    }
    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
        }
        if (camera!=null)
            camera.release();
        camera=null;
        inPreview=false;
        super.onPause();
    }

    private void releaseCameraAndPreview() {

    }


    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width<=width && size.height<=height) {
                if (result==null) {
                    result=size;
                }
                else {
                    int resultArea=result.width*result.height;
                    int newArea=size.width*size.height;

                    if (newArea>resultArea) {
                        result=size;
                    }
                }
            }
        }

        return(result);
    }
    private void startPreview() {
        if (cameraConfigured && camera!=null) {
            camera.startPreview();
            inPreview=true;
        }
    }
    private void initPreview(int width, int height) {
        if (camera!=null && previewHolder.getSurface()!=null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            }
            catch (Throwable t) {
                Log.e("surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast
                        .makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters=camera.getParameters();
                Camera.Size size=getBestPreviewSize(width, height,
                        parameters);

                if (size!=null) {
                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    cameraConfigured=true;
                }
            }
        }
    }

    SurfaceHolder.Callback surfaceCallback=new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            initPreview(width, height);
            startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };
    private void openCamera(){
        if(ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED){
            camera= Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            startPreview();
            preview.setVisibility(View.VISIBLE);
        }
        else {
            requestPermissions(new String[] {android.Manifest.permission.CAMERA},REQUEST_CAMERA_RESULT);
        }
    }
    private static final int REQUEST_CAMERA_RESULT = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case  REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getActivity(), "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                }else {
                    camera= Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    startPreview();
                    preview.setVisibility(View.VISIBLE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    class ContentAdapter extends BaseAdapter {
        UpdateUI updateUI;

        public ContentAdapter(UpdateUI updateUI) {
            this.updateUI = updateUI;
        }

        @Override
        public int getCount() {
            return mListData!=null ? mListData.size():0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_list_word_check, parent, false);
            }
            final TextView textView = (TextView) convertView.findViewById(R.id.text);
//            final Phrase phrase = mListData.get(position);
            if(mListData.get(position).learn==1) {
                textView.setText(mListData.get(position).sentence_jp);
            }
            else
            {
                textView.setVisibility(View.GONE);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count = 0;
                    CountDownText.setText(count + "");
                    updateUI.updateText(position);
                    vn_text1.setText(null);

                }
            });
            return convertView;

        }

    }

    class AnsAdapter extends BaseAdapter {

        List<String> question;
        public AnsAdapter(List<String> question){
            this.question = question;
        }
        @Override
        public int getCount() {
            return this.question.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_asnwer, parent, false);
            }
            final TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(this.question.get(position));

            // Set the border of View (ListView Item)
            convertView.setBackground(getContext().getDrawable(R.drawable.boder_answer));
            textView.setTextSize(20);


            RadioButton rdCheck = (RadioButton) convertView.findViewById(R.id.checkbox);
            rdCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        btAnswer.setEnabled(true);
                        btAnswer.setBackgroundResource(R.drawable.button_login_active);
                    }


                }
            });
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                }
//            });

            return convertView;
        }

    }


}
