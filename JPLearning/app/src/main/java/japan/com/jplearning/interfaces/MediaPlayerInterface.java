package japan.com.jplearning.interfaces;

import android.os.Bundle;

/**
 * Created by HK on 7/12/2017.
 */

public interface MediaPlayerInterface {
    public void onNext(int position,int time);
}
