package japan.com.jplearning.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by HK on 4/12/2017.
 */

public class Chapter {


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    private int id;
    private String name;
    private List<Lesson> lessons;

    public static Chapter fromJson(String s) {
        return new Gson().fromJson(s, Chapter.class);
    }
    public String toString() {
        return new Gson().toJson(this);
    }
}
