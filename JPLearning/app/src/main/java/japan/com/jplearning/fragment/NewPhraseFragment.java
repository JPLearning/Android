package japan.com.jplearning.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;

import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.helper.DatabaseHandler;
import japan.com.jplearning.interfaces.UpdateUI;
import japan.com.jplearning.models.HTesting;
import japan.com.jplearning.models.HWording;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HK on 8/1/2018.
 */

public class NewPhraseFragment extends Fragment implements UpdateUI {

    private SurfaceView preview=null;
    private SurfaceHolder previewHolder=null;
    private Camera camera=null;
    private boolean inPreview=false;
    private boolean cameraConfigured=false;;

    List<String> listRecord;



    @Override
    public void onUpdate() {

    }

    int currentPosition = 0;
    static int count = 0,countQ=0;
    static  int tabNumber = 0;
    @Override
    public void updateText(int position) {
        voiceIcon.setImageResource(R.drawable.micro_black);
        jpText.setText(mListData.get(position).sentence_jp);
        vnText.setText(mListData.get(position).sentence_vi);
        currentPosition = position;
    }

    class Phrase {
        String sentence_jp;
        String sentence_vi;
        String mp3;
        int type;
    }

    class Question {
        String question_text;
        String question_vi;
        String mp3;
        Options options;
        int answer;
    }
    class Options {
        String q1;
        String q2;
        String q3;
    }
    List<Phrase> mListData;
    List<Question> mListQuestion;
    ContentAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listRecord = new ArrayList<>();

    }

    Button webcamButton, btnVi;
    TextView jpText,vnText,CountDownText;
    ImageView voiceIcon,ic_flower,ic_flower1,ic_flower2,
            ic_flower3,ic_flower4,ic_flower5,ic_flower6,ic_flower7,ic_flower8,ic_flower9;
    Button bt1,bt2,bt3,listButton;

    static int question_number = 0;

    TextView text1,text2,text3,question_text,question_vi,num;

    ListView listView;
    static boolean isShow = false;
    ImageView next,prev;
    ProgressBar mProgress;

    ImageView voiceControl;
    RelativeLayout layout12,layout3,q1,q2,q3;
    DatabaseHandler mDB;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_new_layout,
                container, false);
        mDB = new DatabaseHandler(getActivity());
        layout12 = view.findViewById(R.id.layout12);
        layout3 = view.findViewById(R.id.layout3);
        next = view.findViewById(R.id.bt_next);
        prev = view.findViewById(R.id.bt_prev);
        isShow=false;
        listView = (ListView) view.findViewById(R.id.listWord);
        preview=(SurfaceView)view.findViewById(R.id.webcam);
        previewHolder=preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        webcamButton = view.findViewById(R.id.webcam_button);
        jpText = view.findViewById(R.id.jp_text);
        vnText = view.findViewById(R.id.vn_text);
        CountDownText = view.findViewById(R.id.text_count_down_value);
        ic_flower=view.findViewById(R.id.ic_flower);
        ic_flower1=view.findViewById(R.id.ic_flower1);
        ic_flower2=view.findViewById(R.id.ic_flower2);
        ic_flower3=view.findViewById(R.id.ic_flower3);
        ic_flower4=view.findViewById(R.id.ic_flower4);
        ic_flower5=view.findViewById(R.id.ic_flower5);
        ic_flower6=view.findViewById(R.id.ic_flower6);
        ic_flower7=view.findViewById(R.id.ic_flower7);
        ic_flower8=view.findViewById(R.id.ic_flower8);
        ic_flower9=view.findViewById(R.id.ic_flower9);

        text1 = view.findViewById(R.id.text1);
        text2 = view.findViewById(R.id.text2);
        text3 = view.findViewById(R.id.text3);
        num=view.findViewById(R.id.num);
        // question_text = view.findViewById(R.id.question_text);
        question_vi = view.findViewById(R.id.question_text);
        q1 = view.findViewById(R.id.op1);
        q2 = view.findViewById(R.id.op2);
        q3 = view.findViewById(R.id.op3);
        final ImageView icon1 = view.findViewById(R.id.icon1);
        final ImageView icon2 = view.findViewById(R.id.icon2);
        final ImageView icon3 = view.findViewById(R.id.icon3);
        final Button btTV=view.findViewById(R.id.bt_vi);
        final Button nextQuestion = view.findViewById(R.id.next_question);

        webcamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCamera();
                webcamButton.setVisibility(View.GONE);
            }
        });
        count=0;
        voiceIcon = view.findViewById(R.id.bt_micro);
        voiceIcon.setImageResource(R.drawable.micro_black);
        voiceIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voiceIcon.setImageResource(R.drawable.micro_red);
                new Handler().postDelayed(new TimerTask() {
                    @Override
                    public void run() {
                        count++;
                        HWording wording = new HWording();
                        if (mListData.size()>0)
                        {
                            wording.setWord(mListData.get(currentPosition).sentence_jp);
                            wording.setPractice(count);
                        }
                        else
                            wording.setWord(jpText.getText().toString());

                        mDB.addWordHistory(wording,1,AppData.currentLesson);
                        voiceIcon.setImageResource(R.drawable.micro_black);
                        //preview.setVisibility(View.GONE);
                       // webcamButton.setVisibility(View.VISIBLE);
                        //CountDownText.setText(count + "");
                        if(count==1)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                        }
                        else if(count==2)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==3)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==4)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==5)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==6)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower5.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower5.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==7)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower5.setVisibility(View.VISIBLE);
                            ic_flower6.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower5.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower6.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==8)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower5.setVisibility(View.VISIBLE);
                            ic_flower6.setVisibility(View.VISIBLE);
                            ic_flower7.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower5.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower6.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower7.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==9)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower5.setVisibility(View.VISIBLE);
                            ic_flower6.setVisibility(View.VISIBLE);
                            ic_flower7.setVisibility(View.VISIBLE);
                            ic_flower8.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower5.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower6.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower7.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower8.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count==10)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.VISIBLE);
                            ic_flower2.setVisibility(View.VISIBLE);
                            ic_flower3.setVisibility(View.VISIBLE);
                            ic_flower4.setVisibility(View.VISIBLE);
                            ic_flower5.setVisibility(View.VISIBLE);
                            ic_flower6.setVisibility(View.VISIBLE);
                            ic_flower7.setVisibility(View.VISIBLE);
                            ic_flower8.setVisibility(View.VISIBLE);
                            ic_flower9.setVisibility(View.VISIBLE);
                            ic_flower.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower1.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower2.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower3.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower4.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower5.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower6.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower7.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower8.setBackgroundResource(R.drawable.ic_flower );
                            ic_flower9.setBackgroundResource(R.drawable.ic_flower );
                            CountDownText.setBackgroundResource(0);
                        }
                        else if(count>10)
                        {
                            ic_flower.setVisibility(View.VISIBLE);
                            ic_flower1.setVisibility(View.GONE);
                            ic_flower2.setVisibility(View.GONE);
                            ic_flower3.setVisibility(View.GONE);
                            ic_flower4.setVisibility(View.GONE);
                            ic_flower5.setVisibility(View.GONE);
                            ic_flower6.setVisibility(View.GONE);
                            ic_flower7.setVisibility(View.GONE);
                            ic_flower8.setVisibility(View.GONE);
                            ic_flower9.setVisibility(View.GONE);
                            CountDownText.setText(count + "");
                            CountDownText.setBackgroundResource(R.drawable.badge_background);
                        }
                    }
                },500);
            }
        });

        btnVi=view.findViewById(R.id.bt_vn);
        bt1 = view.findViewById(R.id.bt1);
        bt1.setBackgroundResource(R.drawable.button_main);
        bt2 = view.findViewById(R.id.bt2);
        bt2.setBackgroundResource(R.drawable.button_main_selected);
        bt3 = view.findViewById(R.id.bt3);
        layout12.setVisibility(View.VISIBLE);
        layout3.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        next.setVisibility(View.VISIBLE);
        btnVi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnVi.setBackgroundResource(R.drawable.button_selected);
                if (vnText.getText().toString().isEmpty() && currentPosition > -1) {
                    vnText.setText(mListData.get(currentPosition).sentence_vi);
                } else {
                    btnVi.setBackgroundResource(R.drawable.button_webcam);
                    vnText.setText(null);
                }
            }
        });

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setBackgroundResource(R.drawable.button_main_selected);
                bt2.setBackgroundResource(R.drawable.button_main);
                bt3.setBackgroundResource(R.drawable.button_main);
                layout12.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.GONE);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.fragmentLayout, new OnChangeFragment());
                ft.commit();
            }
        });

        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isShow=false;
                listView.setVisibility(View.GONE);
                jpText.setText(mListData.get(0).sentence_jp);
                count=0;
                ic_flower.setVisibility(View.GONE);
                ic_flower1.setVisibility(View.GONE);
                ic_flower2.setVisibility(View.GONE);
                ic_flower3.setVisibility(View.GONE);
                ic_flower4.setVisibility(View.GONE);
                ic_flower5.setVisibility(View.GONE);
                ic_flower6.setVisibility(View.GONE);
                ic_flower7.setVisibility(View.GONE);
                ic_flower8.setVisibility(View.GONE);
                ic_flower9.setVisibility(View.GONE);
                bt1.setBackgroundResource(R.drawable.button_main);
                bt2.setBackgroundResource(R.drawable.button_main_selected);
                bt3.setBackgroundResource(R.drawable.button_main);
                layout12.setVisibility(View.VISIBLE);
                layout3.setVisibility(View.GONE);
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout12.setVisibility(View.GONE);
                bt1.setBackgroundResource(R.drawable.button_main);
                bt2.setBackgroundResource(R.drawable.button_main);
                bt3.setBackgroundResource(R.drawable.button_main_selected);
                icon1.setVisibility(View.GONE);
                icon2.setVisibility(View.GONE);
                icon3.setVisibility(View.GONE);
                nextQuestion.setBackgroundResource(R.drawable.button_login_inactive);
                nextQuestion.setEnabled(false);
                q1.setEnabled(true);
                q2.setEnabled(true);
                q3.setEnabled(true);
                question_vi.setText("**************");
                btTV.setBackgroundResource(R.drawable.button_webcam);
               if (mListQuestion==null || mListQuestion.size()==0){
                    requestData();

               }else {
                   //TODO show question
                   showQuestion();
               }
            }
        });
        mAdapter = new ContentAdapter(this);
        listView.setAdapter(mAdapter);
        if (!isShow)
            listView.setVisibility(View.GONE);
        listButton = (Button)view.findViewById(R.id.listButton);
        listButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation slideLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);
                Animation slideRight = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);

                // listButton.setBackgroundResource(R.drawable.button_selected);
                isShow=!isShow;
                if (isShow) {
                    listView.setVisibility(View.VISIBLE);
                    listView.startAnimation(slideLeft);
                    next.setVisibility(View.GONE);
                    prev.setVisibility(View.GONE);
                }
                else {
                    listView.startAnimation(slideRight);
                    listView.setVisibility(View.GONE);
                    if (currentPosition <mListData.size()-1)
                        next.setVisibility(View.VISIBLE);
                    if(currentPosition>=1)
                        prev.setVisibility(View.VISIBLE);
                   // listButton.setBackgroundResource(R.drawable.button_webcam);
                }

            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next.setVisibility(View.VISIBLE);
                count=0;
                ic_flower.setVisibility(View.GONE);
                ic_flower1.setVisibility(View.GONE);
                ic_flower2.setVisibility(View.GONE);
                ic_flower3.setVisibility(View.GONE);
                ic_flower4.setVisibility(View.GONE);
                ic_flower5.setVisibility(View.GONE);
                ic_flower6.setVisibility(View.GONE);
                ic_flower7.setVisibility(View.GONE);
                ic_flower8.setVisibility(View.GONE);
                ic_flower9.setVisibility(View.GONE);
                CountDownText.setBackgroundResource(0);
                    if (currentPosition<1) {
                    prev.setVisibility(View.GONE);
                }else {
                    currentPosition--;
                    try {
                        playAudio(mListData.get(currentPosition).mp3);
                        if (currentPosition<1) {
                            prev.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    voiceIcon.setImageResource(R.drawable.micro_black);
                    jpText.setText(mListData.get(currentPosition).sentence_jp);
                    vnText.setText(null);
                    btnVi.setBackgroundResource(R.drawable.button_webcam);
                }

            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prev.setVisibility(View.VISIBLE);
                count=0;
                ic_flower.setVisibility(View.GONE);
                ic_flower1.setVisibility(View.GONE);
                ic_flower2.setVisibility(View.GONE);
                ic_flower3.setVisibility(View.GONE);
                ic_flower4.setVisibility(View.GONE);
                ic_flower5.setVisibility(View.GONE);
                ic_flower6.setVisibility(View.GONE);
                ic_flower7.setVisibility(View.GONE);
                ic_flower8.setVisibility(View.GONE);
                ic_flower9.setVisibility(View.GONE);
                CountDownText.setBackgroundResource(0);
                if (currentPosition <mListData.size()-1) {
                    next.setVisibility(View.VISIBLE);
                    currentPosition++;
                    try {
                        playAudio(mListData.get(currentPosition).mp3);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    voiceIcon.setImageResource(R.drawable.micro_black);
                    jpText.setText(mListData.get(currentPosition).sentence_jp);
                    vnText.setText(null);
                    btnVi.setBackgroundResource(R.drawable.button_webcam);
                }else {
                    next.setVisibility(View.GONE);
                    final Dialog d = new Dialog(getActivity());
                    d.setContentView( R.layout.dialog);
                    d.setTitle("よくできました");
                    ImageView iv = (ImageView) d.findViewById(R.id.imageView1);
                    iv.setImageResource(R.drawable.mess);
                    final Button btn = (Button)d.findViewById(R.id.btn);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            d.cancel();
                        }
                    });
                    d.show();
                }
            }
        });

        btTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btTV.setBackgroundResource(R.drawable.button_selected);
                if (question_vi.getText().toString().equals("**************") && currentPosition > -1) {
                    Question question = mListQuestion.get(question_number);
                    question_vi.setText(question.question_vi);
                } else {
                    btTV.setBackgroundResource(R.drawable.button_webcam);
                    question_vi.setText("**************");
                }
            }
        });
        q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListQuestion.get(question_number).answer==0){
                    icon1.setImageResource(R.drawable.ic_success);
                    icon1.setVisibility(View.VISIBLE);
                    icon2.setVisibility(View.VISIBLE);
                    icon3.setVisibility(View.VISIBLE);
                    icon2.setImageResource(R.drawable.ic_incorrect);
                    icon3.setImageResource(R.drawable.ic_incorrect);
                    nextQuestion.setEnabled(true);
                    nextQuestion.setBackgroundResource(R.drawable.button_next);
                    nextQuestion.setText("次へ");
                    q1.setEnabled(false);
                    q2.setEnabled(false);
                    q3.setEnabled(false);
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.sugoi);
                    mMediaPlayer.start();

                }else {
                    icon1.setImageResource(R.drawable.ic_incorrect);
                    icon1.setVisibility(View.VISIBLE);
                    nextQuestion.setEnabled(false);
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.zannen);
                    mMediaPlayer.start();
                }
            }
        });
        q2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q1.setEnabled(true);
                q2.setEnabled(true);
                q3.setEnabled(true);
                nextQuestion.setEnabled(true);
                if (mListQuestion.get(question_number).answer==1){
                    icon2.setImageResource(R.drawable.ic_success);
                    icon1.setVisibility(View.VISIBLE);
                    icon2.setVisibility(View.VISIBLE);
                    icon3.setVisibility(View.VISIBLE);
                    icon1.setImageResource(R.drawable.ic_incorrect);
                    icon3.setImageResource(R.drawable.ic_incorrect);
                    nextQuestion.setEnabled(true);
                    q1.setEnabled(false);
                    q2.setEnabled(false);
                    q3.setEnabled(false);
                    icon3.setEnabled(false);
                    nextQuestion.setBackgroundResource(R.drawable.button_next);
                    nextQuestion.setText("次へ");
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.sugoi);
                    mMediaPlayer.start();
                }else {
                    icon2.setImageResource(R.drawable.ic_incorrect);
                    icon2.setVisibility(View.VISIBLE);
                    nextQuestion.setEnabled(false);
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.zannen);
                    mMediaPlayer.start();
                }
            }
        });
        q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q1.setEnabled(true);
                q2.setEnabled(true);
                q3.setEnabled(true);
                nextQuestion.setEnabled(true);
                if (mListQuestion.get(question_number).answer==2){
                    icon3.setImageResource(R.drawable.ic_success);
                    icon1.setVisibility(View.VISIBLE);
                    icon2.setVisibility(View.VISIBLE);
                    icon3.setVisibility(View.VISIBLE);
                    icon1.setImageResource(R.drawable.ic_incorrect);
                    icon2.setImageResource(R.drawable.ic_incorrect);
                    nextQuestion.setEnabled(true);
                    q1.setEnabled(false);
                    q2.setEnabled(false);
                    q3.setEnabled(false);
                    icon2.setEnabled(false);
                    nextQuestion.setBackgroundResource(R.drawable.button_next);
                    nextQuestion.setText("次へ");
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.sugoi);
                    mMediaPlayer.start();
                }else {
                    icon3.setImageResource(R.drawable.ic_incorrect);
                    icon3.setVisibility(View.VISIBLE);
                    nextQuestion.setEnabled(false);
                    MediaPlayer mMediaPlayer = MediaPlayer.create(getActivity(), R.raw.zannen);
                    mMediaPlayer.start();
                }
            }
        });

        nextQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                q1.setEnabled(true);
                q2.setEnabled(true);
                q3.setEnabled(true);
                question_vi.setText("**************");
                nextQuestion.setText("");
                nextQuestion.setBackgroundResource(R.drawable.button_login_inactive);
                nextQuestion.setEnabled(false);
                question_number++;
                countQ=question_number+1;
                num.setText(""+countQ);
                HTesting test = new HTesting();
                test.setQuestion("問題 "+question_number +AppData.currentLesson);
                test.setStatus(1);
                mDB.addHistoryTest(test,AppData.currentLesson);
                if (question_number<mListQuestion.size()) {
                    showQuestion();
                    icon1.setVisibility(View.GONE);
                    icon2.setVisibility(View.GONE);
                    icon3.setVisibility(View.GONE);

                }else {
                    question_number = 0;
                    num.setText("");
                    final Dialog d = new Dialog(getActivity());
                    d.setContentView( R.layout.dialog);
                    d.setTitle("よくできました");
                    ImageView iv = (ImageView) d.findViewById(R.id.imageView1);
                    iv.setImageResource(R.drawable.mess);
                    final Button btn = (Button)d.findViewById(R.id.btn);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            d.cancel();
                            showQuestion();
                            countQ=question_number+1;
                            num.setText(""+countQ);
                            icon1.setVisibility(View.GONE);
                            icon2.setVisibility(View.GONE);
                            icon3.setVisibility(View.GONE);
                        }
                    });
                    d.show();
                }
            }
        });
        mProgress = view.findViewById(R.id.progress_bar);

        voiceControl = view.findViewById(R.id.image_voice);
        voiceControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voiceControl.setBackgroundResource(R.drawable.round_voice_active);
                new Handler().postDelayed(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            playAudio(mListQuestion.get(question_number).mp3);
                        }catch (Exception e){

                        }
                    }
                },1000);
            }
        });

        return view;
    }
    MediaPlayer mediaPlayer;
    private void playAudio(String url) throws Exception {
        killMediaPlayer();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setDataSource(url);
        mediaPlayer.prepare();
        mediaPlayer.start();
        new Handler().postDelayed(new TimerTask() {
            @Override
            public void run() {
                voiceControl.setBackgroundResource(R.drawable.round_voice);
            }
        },2000);
    }
    private void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void requestData(){
        mProgress.setVisibility(View.VISIBLE);
        Map<String, String> map = new HashMap<>();
        map.put("lesson_id", AppData.currentLesson+"");
        APIUtils.getMarcoService().getListQuestBank(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                mProgress.setVisibility(View.GONE);
                if (response.code()==200){

                    try {
                        JsonArray jsonArray = response.body().getAsJsonObject("data").getAsJsonArray("questions");
                        if (jsonArray != null) {
                            mListQuestion = new ArrayList<>();
                            for (int i=0;i<jsonArray.size();i++){
                                Question q = new Question();
                                JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                               // q.question_text = jsonObject.get("question").getAsString();
                                q.question_vi = jsonObject.get("question_vi").getAsString();
                                q.mp3 = jsonObject.get("mp3").getAsString();
                                q.options  = new Options();
                                JsonArray arrayAns = jsonObject.get("answers").getAsJsonArray();
                                for (int j=0;j<arrayAns.size();j++){
                                    switch(j){
                                        case 0:
                                            q.options.q1 = arrayAns.get(j).getAsJsonObject().get("answer").getAsString();
                                            if (arrayAns.get(j).getAsJsonObject().get("yes").getAsInt()==1)
                                                q.answer = 0;
                                            break;
                                        case 1:
                                            q.options.q2 = arrayAns.get(j).getAsJsonObject().get("answer").getAsString();
                                            if (arrayAns.get(j).getAsJsonObject().get("yes").getAsInt()==1)
                                                q.answer = 1;
                                            break;
                                        case 2:
                                            q.options.q3 = arrayAns.get(j).getAsJsonObject().get("answer").getAsString();
                                            if (arrayAns.get(j).getAsJsonObject().get("yes").getAsInt()==1)
                                                q.answer = 2;
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                mListQuestion.add(q);

                            }
                        }
                    } catch (Exception ex) {

                    }
                    if (mListQuestion!=null && mListQuestion.size()>0)
                        showQuestion();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mProgress.setVisibility(View.GONE);
            }
        });
    }
    private void showQuestion(){
        layout3.setVisibility(View.VISIBLE);
        Question question = mListQuestion.get(question_number);
       // question_text.setText(question.question_text);
        text1.setText("Q1. "+question.options.q1);
        text2.setText("Q2. "+question.options.q2);
        text3.setText("Q3. "+question.options.q3);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        map.put("lesson_id", AppData.currentLesson+"");
        APIUtils.getMarcoService().getListPhraseToLearn(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        if (jsonArray != null) {
                            Gson gson = new Gson();
                            String jsonOutput = jsonArray.toString();
                            Type listType = new TypeToken<List<Phrase>>(){}.getType();
                            mListData = (List<Phrase>) gson.fromJson(jsonOutput, listType);
                            if (jpText!=null && mListData!=null && mListData.size()>0 )
                                jpText.setText(mListData.get(0).sentence_jp);
                            if (mAdapter!=null)
                                mAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception ex) {

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        //openCamera();
    }
    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
        }
        if (camera!=null)
            camera.release();
        camera=null;
        inPreview=false;
        super.onPause();
    }

    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result=null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width<=width && size.height<=height) {
                if (result==null) {
                    result=size;
                }
                else {
                    int resultArea=result.width*result.height;
                    int newArea=size.width*size.height;

                    if (newArea>resultArea) {
                        result=size;
                    }
                }
            }
        }

        return(result);
    }
    private void startPreview() {
        if (cameraConfigured && camera!=null) {
            camera.startPreview();
            inPreview=true;
        }
    }
    private void initPreview(int width, int height) {
        if (camera!=null && previewHolder.getSurface()!=null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            }
            catch (Throwable t) {
                Log.e("surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast
                        .makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG)
                        .show();
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters=camera.getParameters();
                Camera.Size size=getBestPreviewSize(width, height,
                        parameters);

                if (size!=null) {
                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    cameraConfigured=true;
                }
            }
        }
    }

    SurfaceHolder.Callback surfaceCallback=new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            initPreview(width, height);
            startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };
    private void openCamera(){
        if(ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED){
            camera= Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            startPreview();
            preview.setVisibility(View.VISIBLE);
        }
        else {
            requestPermissions(new String[] {android.Manifest.permission.CAMERA},REQUEST_CAMERA_RESULT);
        }
    }
    private static final int REQUEST_CAMERA_RESULT = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case  REQUEST_CAMERA_RESULT:
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getActivity(), "Cannot run application because camera service permission have not been granted", Toast.LENGTH_SHORT).show();
                }else {
                    camera= Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                    startPreview();
                    preview.setVisibility(View.VISIBLE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    class ContentAdapter extends BaseAdapter {
        UpdateUI updateUI;

        public ContentAdapter(UpdateUI updateUI) {
            this.updateUI = updateUI;
        }

        @Override
        public int getCount() {
            return mListData!=null ? mListData.size():0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_list_word_check, parent, false);
            }
            final TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(mListData.get(position).sentence_jp);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count = 0;
                    CountDownText.setBackgroundResource(0);
                  //  CountDownText.setText(count + "");
                    ic_flower.setVisibility(View.GONE);
                    ic_flower1.setVisibility(View.GONE);
                    ic_flower2.setVisibility(View.GONE);
                    ic_flower3.setVisibility(View.GONE);
                    ic_flower4.setVisibility(View.GONE);
                    ic_flower5.setVisibility(View.GONE);
                    ic_flower6.setVisibility(View.GONE);
                    ic_flower7.setVisibility(View.GONE);
                    ic_flower8.setVisibility(View.GONE);
                    ic_flower9.setVisibility(View.GONE);
                    try {
                        playAudio(mListData.get(position).mp3);
                        Animation slideRight = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out_right);
                        listView.startAnimation(slideRight);
                        listView.setVisibility(View.GONE);
                        isShow=false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    updateUI.updateText(position);
                    next.setVisibility(View.VISIBLE);
                    if(currentPosition>0)
                        prev.setVisibility(View.VISIBLE);
                    vnText.setText(null);

                }
            });


            return convertView;

        }

    }

    class AnsAdapter extends BaseAdapter {

        List<String> question;
        public AnsAdapter(List<String> question){
            this.question = question;
        }
        @Override
        public int getCount() {
            return this.question.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_asnwer, parent, false);
            }
            final TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(this.question.get(position));
            RadioButton rdCheck = (RadioButton) convertView.findViewById(R.id.checkbox);
            rdCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        //btAnswer.setEnabled(true);
                        //btAnswer.setBackgroundResource(R.drawable.button_login_active);
                    }


                }
            });
//            convertView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                }
//            });

            return convertView;
        }

    }
}
