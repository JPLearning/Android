package japan.com.jplearning.models;

import java.util.Date;

/**
 * Created by HK on 22/1/2018.
 */

public class HTesting {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String question;
    private int status;
    private Date startDate;
    private Date endDate;
    private String comment;

    public HTesting(int id,String question,int status, Date startDate, Date endDate,String comment){
        this.question = question;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
        this.comment=comment;
    }
    public HTesting(){}

}
