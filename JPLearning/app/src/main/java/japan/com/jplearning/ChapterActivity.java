package japan.com.jplearning;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import japan.com.jplearning.fragment.ChapterFragment;
import japan.com.jplearning.fragment.ContentDetailFragment;
import japan.com.jplearning.LoginActivity;

/**
 * Created by mac on 10/20/17.
 */

public class ChapterActivity extends AppCompatActivity {

    String tag = "ChapterActivity";
    ImageView buttonBack;
    TextView txtTitleText;
    public static int chapter_id = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        buttonBack = (ImageView) findViewById(R.id.back);
        txtTitleText = (TextView) findViewById(R.id.textTitle);
        FragmentManager fm = getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragmentLayout, new ChapterFragment());
        ft.commit();
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment f = getFragmentManager().findFragmentById(R.id.fragmentLayout);
                if (f instanceof ContentDetailFragment){
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragmentLayout, new ChapterFragment())
                            .commit();
                    findViewById(R.id.textTitle).setVisibility(View.VISIBLE);
                    findViewById(R.id.textTitle1).setVisibility(View.GONE);
                }else {
                    //onBackPressed();
                    Intent myIntent = new Intent(ChapterActivity.this, LoginActivity.class);
                    startActivity(myIntent);
                }
            }
        });
    }
    public void setTitle(String text){
        findViewById(R.id.textTitle).setVisibility(View.GONE);
        TextView title = (TextView) findViewById(R.id.textTitle1);
        title.setText(text);
        title.setVisibility(View.VISIBLE);
    }
  //todo change txt and set visibility
   public  void changeScreenAndSetMain(int i)
   {
       if (i == 0)
       {
           buttonBack.setVisibility(View.GONE);
           txtTitleText.setText("日常会話");
       }
       else
       {
           buttonBack.setVisibility(View.VISIBLE);
           txtTitleText.setText("日常会話>第２部　かいものする");
       }
   }
}
