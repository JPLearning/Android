package japan.com.jplearning.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import japan.com.jplearning.PlayMusicActivity;
import japan.com.jplearning.PlayVideoActivity;
import japan.com.jplearning.R;
import japan.com.jplearning.customview.CustomVideoView;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.helper.DatabaseHandler;
import japan.com.jplearning.helper.StringUtils;
import japan.com.jplearning.models.HLesson;
import japan.com.jplearning.models.Lesson;
import japan.com.jplearning.models.LessonStudy;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by HK on 10/10/2017.
 */

public class LessonFragment extends Fragment {

    int position = 0;
    private LinearLayout mSubLayout;
    CustomVideoView videoView;
    SeekBar mSeekBar;
    List<Subtitle> mListSub;
    ListView mListContent;
    Context mContext;
    private ContentAdapter mAdapter;
    LinearLayout headertitle;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListSub = new ArrayList<>();
        loadSurfFile();
    }

    @Override
    public void onPause() {
        super.onPause();
        videoView.pause();
    }



    private void killMediaPlayer() {
        if ( videoView!= null) {
            try {
                videoView.destroyDrawingCache();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        killMediaPlayer();
        lStudy.setStatus(progress);
        DateFormat df = new DateFormat();
        lStudy.setEndDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
        APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        videoView.pause();
        videoView.destroyDrawingCache();
    }

    TextView mTextStartTime;
    TextView mEndTime;
   // MediaController mc;
    ImageView playPauseImage;

    private LessonStudy lStudy = null;
    DatabaseHandler mDB;
    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }
    int progress = 2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDB = new DatabaseHandler(getActivity());
        final View view = inflater.inflate(R.layout.fragment_lesson,
                container, false);
   //     TextView videoTitle = view.findViewById(R.id.video_title);
//        videoTitle.setText(AppData.videoName);

        mSubLayout = (LinearLayout) view.findViewById(R.id.has_sub_layout);

        videoView = (CustomVideoView) view.findViewById(R.id.video_view);
        mSeekBar = (SeekBar)view.findViewById(R.id.seekbarStatus);
        mListContent = (ListView)view.findViewById(R.id.listContent);
        mAdapter = new ContentAdapter();
        mListContent.setAdapter(mAdapter);
        headertitle=(LinearLayout)view.findViewById(R.id.headertitle);

        String path = null;
        if (AppData.currentChapter==2) {
            if (AppData.currentLesson==2 )
                path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_0_0;
            else
                path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_0_1;
        }
        else if (AppData.currentChapter==1)
        {
            if(AppData.currentLesson==1)
               path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_1_1;
        else if(AppData.currentLesson==8)
              path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_1_0;
            else if(AppData.currentLesson==3)
                path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_1_2;
        }
        else if(AppData.currentChapter==3){
            path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.sakura;
            headertitle.setVisibility(View.GONE);
        }
        else if (AppData.currentChapter==0 )
        {
            Intent nextClass = new Intent(getActivity(), PlayVideoActivity.class);
            startActivity(nextClass);
        }
//        else  if(AppData.currentChapter==3)
//        {
//            Intent nextClass = new Intent(getActivity(), PlayMusicActivity.class);
//            startActivity(nextClass);
//        }
//        else if (AppData.currentChapter==0)
//        {
//            path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.new_video;
//        }

        videoView.setVideoURI(Uri.parse(path));

       // mc = new MediaController(getActivity());

        videoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
            @Override
            public void onPlay() {
                mSeekBar.postDelayed(onEverySecond, 100);
            }

            @Override
            public void onPause() {
                //mSeekBar.postDelayed(onEverySecond, 100);
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                mp.pause();
                //mc.show(1);
                mListContent.smoothScrollToPosition(0);
                mListContent.setSelection(0);
                progress = 1;//complete

                final Dialog d = new Dialog(getActivity());
                d.setContentView( R.layout.dialog);
                d.setTitle("よくできました");
                ImageView iv = (ImageView) d.findViewById(R.id.imageView1);
                iv.setImageResource(R.drawable.mess);
                final Button btn = (Button)d.findViewById(R.id.btn);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        d.cancel();
                    }
                });
                d.show();
                HLesson lesson = new HLesson();
                lesson.setTitle(AppData.currentLessonName);
                lesson.setListen(1);
                mDB.addHistoryLesson(lesson,AppData.currentLesson);
            }
        });

        videoView.start();
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                if(fromUser) {
//                     this is when actually seekbar has been seeked to a new position
                    int time = videoView.getDuration();
                    if (videoView.isPlaying()) {
                        videoView.pause();
                        videoView.seekTo(progress + 1500);
                        videoView.start();
                    }else {
                        videoView.seekTo(progress + 000);
                        videoView.pause();
                    }
//                    int totalDuration = videoView.getDuration();
//                    int currentPosition = Utilities.progressToTimer(seekBar.getProgress(), totalDuration);
//
//                    // forward or backward to certain seconds
//                    if (videoView.isPlaying()) {
//                        videoView.seekTo(currentPosition);
//                        videoView.start();
//                    }else
//                        videoView.seekTo(currentPosition);

//
                }
            }

        });
        mTextStartTime = view.findViewById(R.id.text_start_time);
        mEndTime = view.findViewById(R.id.text_end_time);
        playPauseImage = view.findViewById(R.id.image_modify_pause);

        return view;

    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DateFormat df = new DateFormat();

        lStudy = new LessonStudy();
        lStudy.setStartDate(df.format("yyyy-MM-dd hh:mm:ss", new Date()).toString());
        if(AppData.mUser!=null)
            lStudy.setAccess_token(AppData.mUser.getAccess_token());
        lStudy.setLesson_id(AppData.currentLesson);
        lStudy.setType(1);
        APIUtils.getMarcoService().lessonUpdate(lStudy).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //start time .
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (position == 0) {
                    try {
                        videoView.requestFocus();
                        videoView.resizeVideoView(mSeekBar.getWidth(),(int) convertDpToPixel(400,getActivity()));

                        mSeekBar.setMax(videoView.getDuration());
                        mSeekBar.postDelayed(onEverySecond, 100);
                        updateStatus();
//                        videoView.setMediaController(mc);
//
//                        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
//                        lp.gravity = Gravity.BOTTOM;
//                        mc.setLayoutParams(lp);
//
//                        ((ViewGroup) mc.getParent()).removeView(mc);
//
//                        ((FrameLayout) view.findViewById(R.id.videoControllerLayout)).addView(mc);
//                        mediaPlayer.start();
//                        mc.show(0);
                        // readSubtitle(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.sublite));
                        mEndTime.setText(StringUtils.duration2String(videoView.getDuration()));
                    } catch (Exception e) {
                        System.out.printf("Error playing video %s\n", e);
                    }
                } else {
                    videoView.pause();
                }

            }
        });
        playPauseImage.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (!videoView.isPlaying()){
                    videoView.start();
                    playPauseImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable
                            .ic_pause_menu_modify_player));

                }else {
                    videoView.pause();
                    playPauseImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable
                            .ic_play_arrow_white));
                }
            }
        });
    }

    class ContentAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mListSub.size();
        }

        @Override
        public Object getItem(int position) {
            return mListSub.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null){
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                convertView = layoutInflater.inflate(R.layout.item_text_sub,parent,false);
            }
            TextView textView = (TextView)convertView.findViewById(R.id.text);
            textView.setText(mListSub.get(position).text);
            if (mListSub.get(position).isReading)
                textView.setTextColor(Color.parseColor("#FF0000"));
            else
                textView.setTextColor(Color.parseColor("#000000"));

            return convertView;
        }
    }
    private void updateStatus(){

        Resources r = getResources();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;


       // float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, r.getDisplayMetrics());
        int widthsc  = mSeekBar.getWidth();

        long pivious = 0;
        for (Subtitle subtitle: mListSub){
            //add position
//            String[] times = subtitle.time.split(" --> ");
//            String startTime = times[0];
//            String endTime = times[1];
//            long startTimeL = dateParseRegExp(startTime);
//            long endTimeL = dateParseRegExp(endTime);
//            long duration = endTimeL - startTimeL;

            float width  = widthsc * ((float)subtitle.duration/videoView.getDuration());

            Log.e("Width",width+"");

            View view = new View(getActivity());
            view.setBackgroundColor(Color.parseColor("#5cb85c"));

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()),
                    ViewGroup.LayoutParams.MATCH_PARENT);
            if (pivious>0) {
                float marginWidth = widthsc * ((float) (subtitle.startTime-pivious)/videoView.getDuration());
                lp.setMargins((int)marginWidth,0,0,0);
            }
            pivious = subtitle.startTime;

            mSubLayout.addView(view,lp);
        }
    }

    private static Pattern pattern = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3})");

    public static long dateParseRegExp(String period) {
        Matcher matcher = pattern.matcher(period);
        if (matcher.matches()) {
            return Long.parseLong(matcher.group(1)) * 3600000L
                    + Long.parseLong(matcher.group(2)) * 60000
                    + Long.parseLong(matcher.group(3)) * 1000
                    + Long.parseLong(matcher.group(4));
        } else {
            throw new IllegalArgumentException("Invalid format " + period);
        }
    }
    private Runnable onEverySecond=new Runnable() {
        @SuppressLint("NewApi")
        @Override
        public void run() {
            if (getContext()==null)
                return;
            if(mSeekBar != null) {
                mSeekBar.setProgress(videoView.getCurrentPosition());
            }
            //TODO update text view if have new conversation

            int currentTime = videoView.getCurrentPosition();
            updateListView(currentTime);
            if(videoView.isPlaying()) {
                mSeekBar.postDelayed(onEverySecond, 100);
                playPauseImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable
                        .ic_pause_menu_modify_player));

            } else {
                playPauseImage.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable
                        .ic_play_arrow_white));
            }
            mTextStartTime.setText(StringUtils.duration2String(currentTime));
        }
    };
    private void updateListView(long currentTime){
        int i=0;
        int pos = -1;
        for (Subtitle subtitle:mListSub){
            i++;
            if (subtitle.startTime<= currentTime && (subtitle.startTime + subtitle.duration)>= currentTime ) {
                subtitle.isReading = true;
                pos = i;
            }else
                subtitle.isReading = false;
        }
        mAdapter.notifyDataSetChanged();

        if (pos>0) {
            smoothScrollToPositionFromTop(mListContent,pos-1);


        }
//        if (pos > mListSub.size()-4) {
//            mListContent.post(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            });
//           // mListContent.smoothScrollToPosition(pos + 1);
//        }


    }

    public static void smoothScrollToPositionFromTop(final AbsListView view, final int position) {
        View child = getChildAtPosition(view, position);
        // There's no need to scroll if child is already at top or view is already scrolled to its end
        if ((child != null) && ((child.getTop() == 0) || ((child.getTop() > 0) && !view.canScrollVertically(1)))) {
            return;
        }

        view.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(final AbsListView view, final int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    view.setOnScrollListener(null);

                    // Fix for scrolling bug
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            view.setSelection(position);
                        }
                    });
                }
            }

            @Override
            public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) {
            }
        });

        // Perform scrolling to position
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                view.smoothScrollToPositionFromTop(position, 0);
            }
        });
    }
    public static View getChildAtPosition(final AdapterView view, final int position) {
        final int index = position - view.getFirstVisiblePosition();
        if ((index >= 0) && (index < view.getChildCount())) {
            return view.getChildAt(index);
        } else {
            return null;
        }
    }

    private void loadSurfFile(){
        //TODO

        Scanner s = null;
        if (AppData.currentChapter==2) {
            if (AppData.currentLesson==2)
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_0));
            else
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_0_1));
        }
        else if (AppData.currentChapter == 1){
            if(AppData.currentLesson==1)
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_1_1));
            else if(AppData.currentLesson==8)
            s = new Scanner(getResources().openRawResource(R.raw.subtitle_1_0));
            else if(AppData.currentLesson== 3)
                s = new Scanner(getResources().openRawResource(R.raw.subtitle_1_2));
        }
//        else if(AppData.currentChapter==3){
//            s = new Scanner(getResources().openRawResource(R.raw.subtitle_3_0));
//        }
        if (s==null)
            return;
        int number =0;
        try {
            while (s.hasNextLine()) {
                number++;
                String word = s.nextLine();
                if (number<=2)
                    continue;
                Subtitle subtitle = new Subtitle();
                String time = s.nextLine();
                if (time.equals(""))
                    continue;
                subtitle.time = time;
                subtitle.text = s.nextLine();
                String[] times = subtitle.time.split(" --> ");
                String startTime = times[0];
                String endTime = times[1];
                subtitle.startTime = dateParseRegExp(startTime);
                long endTimeL = dateParseRegExp(endTime);
                subtitle.duration = endTimeL - subtitle.startTime;
                s.nextLine();
                mListSub.add(subtitle);
            }
        } finally {
            s.close();
        }
    }
    class Subtitle {

        String time;
        long startTime;
        long duration;
        String text;
        boolean isReading = false;
    }

}
