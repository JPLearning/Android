package japan.com.jplearning.models;

import java.util.Date;

/**
 * Created by HK on 22/1/2018.
 */

public class HLesson {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSpeak() {
        return speak;
    }

    public void setSpeak(int speak) {
        this.speak = speak;
    }

    public int getLearn() {
        return learn;
    }

    public void setLearn(int learn) {
        this.learn = learn;
    }

    public int getListen() {
        return listen;
    }

    public void setListen(int listen) {
        this.listen = listen;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String title;
    private int speak;
    private int learn;
    private int listen;
    private Date startDate;
    private Date endDate;
    private String comment;


    public HLesson(int id,String title,int speak,int learn,int listen, Date startDate, Date endDate,String comment){
        this.title = title;
        this.speak = speak;
        this.learn = learn;
        this.listen = listen;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
        this.comment=comment;
    }
    public HLesson(){}
}
