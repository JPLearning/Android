package japan.com.jplearning.interfaces;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import japan.com.jplearning.models.ClientUser;

import japan.com.jplearning.models.LessonStudy;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by Dell on 6/8/2017.
 */

public interface JPLearningAPI {

    @POST("frontendUsers/login")
    Call<Void> getListUser(@Body JsonObject postData, Callback<JsonObject> callback);

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<JsonObject> doLogin(@Body ClientUser postData);

    @Headers("Content-Type: application/json")
    @POST("lessonUpdate")
    Call<JsonObject> lessonUpdate(@Body LessonStudy postData);

    @GET("getListLessonByChapterID")
    Call<JsonObject> getListLessonByChapterID(@QueryMap Map<String, String> params);

    @GET("getContentByid")
    Call<JsonObject> getListChapters(@QueryMap Map<String, String> params);

    @GET("conversation")
    Call<JsonObject> getListContentByLesson(@QueryMap Map<String, String> params);
    @GET("phraseLearn")
    Call<JsonObject> getListPhraseToLearn(@QueryMap Map<String, String> params);
    @GET("listword")
    Call<JsonObject> getListWordByLesson(@QueryMap Map<String, String> params);

    @GET("listQA")
    Call<JsonObject> getListQuestBank(@QueryMap Map<String, String> params);

//    @Multipart
//    @POST("storages/avatar/upload")
//    Call<List<MarcoImage>> uploadAvartar(@Header("Authorization") String authorization, @Part MultipartBody.Part filePart, @Part("owner") RequestBody ownerID);
//
//    @Multipart
//    @POST("storages/chat/upload")
//    Call<List<MarcoImage>> uploadChatAttachImage(@Header("Authorization") String authorization, @Part MultipartBody.Part filePart, @Part("owner") RequestBody ownerID);
//
//    @Multipart
//    @POST("storages/listing/upload")
//    Call<List<MarcoImage>> uploadListingImage(@Header("Authorization") String authorization, @Part MultipartBody.Part filePart, @Part("owner") RequestBody ownerID);
//
//
//    @FormUrlEncoded
//    @POST("frontendUsers/reset")
//    Call<ResponseBody> resetPassword(@Field("userEmail") String email);
//
//    @POST("frontenduserreports")
//    Call<ResponseBody> doReport(@Header("Authorization") String access_token, @Body Report report);
//
//    @PATCH
//    Call<MarcoUser> updateProfile(@Url String url, @Header("Authorization") String access_token, @Body MarcoUser postData);
//
//
//    @GET
//    Call<ResponseBody> findUserEmail(@Url String fullUrl);
//
//    @GET
//    Call<ResponseBody> getLocationAddress(@Url String fullUrl);
//
//    @GET
//    Call<ResponseBody> logoutInstagram(@Url String fullUrl);
//



//    @GET("categories")
//    Call<List<Category>> getListCategory();
//
//    @GET("listings")
//    Call<List<MarcoItem>> searchListing(@QueryMap Map<String, String> params);
//
//    @GET("frontendUserFavourites")
//    Call<List<MarcoItem>> getAllFavouriteByUser(@QueryMap Map<String, String> params);
//
//    @POST("frontendUserFavourites")
//    Call<ResponseBody> createFavourite(@Header("Authorization") String access_token, @Body Favourite favourite);
//
//
//    @HTTP(method = "DELETE",hasBody = true)
//    Call<ResponseBody> deleteFavourite(@Url String url, @Body Map<String, String> params);
//
//
//
//    @GET("pushNotificationEmails")
//    Call<List<Notification>> getNotification(@QueryMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST("frontendUsers/change-password")
//    Call<ResponseBody> changePassword(@Header("Authorization") String access_token, @FieldMap Map<String, String> params);
//
//    @POST
//    Call<ResponseBody> markAllRead(@Url String url, @Body Map<String, Object> body);
//
//    @POST
//    Call<JsonObject> createGroup(@Url String url, @HeaderMap Map<String, String> headerParams, @Body FCModel fcModel);
//
//    @DELETE
//    Call<ResponseBody> delListingByOwner(@Header("Authorization") String access_token, @Url String url);
//
//    @POST("listings/sendRequestToJoinMarket")
//    Call<ResponseBody> sendRequestToJoin(@Header("Authorization") String access_token, @Body Map<String, Object> body);
//
//    @POST("listings/sendInvitationToJoinMarket")
//    Call<ResponseBody> sendInvitationToJoinMarket(@Header("Authorization") String access_token, @Body Map<String, Object> body);
//
//
//    @POST("listings/updateInvitationStatus")
//    Call<ResponseBody> updateInvitationStatus(@Header("Authorization") String access_token, @Body Map<String, Object> body);
//
//    @POST("listings/updateJoiningRequestStatus")
//    Call<ResponseBody> updateJoiningRequestStatus(@Header("Authorization") String access_token, @Body Map<String, Object> body);
//
//    @GET
//    Call<MarcoItem> getListingById(@Url String url);//listing/@id
//
//    @GET("messages/getChatThreads")
//    Call<ResponseBody>  getChatThreads();
//
//    @GET("messages")
//    Call<ResponseBody> getChatHistory(@QueryMap Map<String, String> params);
//
//    @GET("messages/getChatThreads")
//    Call<List<ChatListModel>> getChatThreads(@QueryMap Map<String, String> params);
//
//    @GET("messages/countUnreadMessages")
//    Call<ResponseBody> getUnreadCount(@QueryMap Map<String, String> params);
//
//    @HTTP(method = "DELETE",hasBody = true)
//    Call<ResponseBody> deleteChatThread(@Url String url, @Body Map<String, Object> params);
//
//    @GET("messages/getChatHistory")
//    Call<List<ChatMessageModel>> loadChatHistory(@QueryMap Map<String, String> params);
//
//    @GET
//    Call<MarcoUser> getUserProfile(@Url String url, @QueryMap Map<String, Object> params);
//
//    @POST
//    Call<ResponseBody> addListing(@Url String url, @Body MarcoItem marcoItem);
//
//    @PATCH
//    Call<ResponseBody> updateListing(@Url String url, @Body MarcoItem marcoItem);
}
