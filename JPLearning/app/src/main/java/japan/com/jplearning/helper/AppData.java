package japan.com.jplearning.helper;

import java.util.List;

import japan.com.jplearning.models.Chapter;
import japan.com.jplearning.models.ClientUser;

/**
 * Created by HK on m2/11/2017.
 */

public class AppData {
    public static int currentChapter;
    public static int currentLesson;
    public static String currentChapterName;
    public static String currentLessonName;
    public static String videoName;
    public static ClientUser mUser; // store session
    public static List<Chapter> mChapters = null;
}
