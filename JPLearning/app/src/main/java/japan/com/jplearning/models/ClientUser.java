package japan.com.jplearning.models;

import com.google.gson.JsonObject;

/*
 * Created by HK on 27/11/2017.
 */

public class ClientUser {
    private String name;
    private String password;
    private String email;
    private int id;
    private String access_token;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public static ClientUser createUserFromJsonData(JsonObject jsonObject){
        if (jsonObject==null)
                return null;
        ClientUser user = new ClientUser();
        user.id = jsonObject.get("id").getAsInt();
        user.email = jsonObject.get("email").getAsString();
        user.name = jsonObject.get("name").getAsString();
        user.access_token = jsonObject.get("access_token").getAsString();
        return  user;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
