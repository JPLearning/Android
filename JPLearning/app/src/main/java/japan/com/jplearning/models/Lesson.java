package japan.com.jplearning.models;

import java.util.List;

/**
 * Created by HK on 4/12/2017.
 */

public class Lesson {
    private int id;
    private String name;
    private List<Video> videos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }
}
