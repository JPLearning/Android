package japan.com.jplearning.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import ak.sh.ay.musicwave.MusicWave;
import japan.com.jplearning.R;
import japan.com.jplearning.helper.AppData;
import japan.com.jplearning.helper.DatabaseHandler;
import japan.com.jplearning.interfaces.MediaPlayerInterface;
import japan.com.jplearning.models.HLesson;
import japan.com.jplearning.utils.APIUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by HK on 5/12/2017.
 */

public class ConversationFragment extends Fragment implements MediaPlayerInterface {
    int type_id;
    int mCurrentPosition = 0, temp = 0;
    Timer countDownTimer;
    static boolean isFirst = true, isPaused = false;
    private boolean clicked;
    ///////
    static Random rnd = new Random();
    private MediaRecorder mRecorder;
    private String outputFile;
    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private MediaPlayer mediaPlayer;
    ////////
    private ImageButton recordbtn;
    private TextView tv;
    private ProgressBar progressBar;
    private int i = 0;
    public int totalDur = 2000,t=3000;
    //////

       @Override
    public void onNext(final int position, final int time) {
        countDownTimer = new Timer();
        if (getActivity() != null) {
            try {
                countDownTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        if (getActivity() == null) {
                            return;
                        }
                        getActivity().runOnUiThread(new TimerTask() {
                            @Override
                            public void run() {
                                mCurrentPosition++;
                                mListView.smoothScrollToPosition(mCurrentPosition);
                                if (mCurrentPosition >= mListData.size()) {
                                    recordbtn.setEnabled(false);
                                    countDownTimer.cancel();
                                    mCurrentPosition = 0;
                                    HLesson lesson = new HLesson();
                                    lesson.setTitle(AppData.currentLessonName);
                                    lesson.setSpeak(1);
                                    mDB.addHistoryLesson(lesson, AppData.currentLesson);
                                    final Dialog d = new Dialog(getActivity());
                                    d.setContentView(R.layout.dialog);
                                    d.setTitle("よくできました");
                                    ImageView iv = (ImageView) d.findViewById(R.id.imageView1);
                                    iv.setImageResource(R.drawable.mess);
                                    recordbtn.setVisibility(View.GONE);
                                    tv.setText("");
                                    final Button btn = (Button) d.findViewById(R.id.btn);
                                    btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            d.cancel();
                                        }
                                    });
                                    d.show();
                                } else {
                                    if (mListData.get(mCurrentPosition).type == type_id) {
                                        recordbtn.setEnabled(true);
                                        countDownTimer.cancel();
                                        btnPlay.setVisibility(View.VISIBLE);
                                        tv.setText("マイクをタップして発音してください");
                                    } else {
                                        try {
                                            playAudio(prefix + (mCurrentPosition + 1));
                                            recordbtn.setEnabled(false);
                                            progressBar.setVisibility(View.GONE);
                                            recordbtn.setImageResource(R.drawable.new_voice);
                                            recordbtn.setEnabled(false);
                                            recordbtn.setVisibility(View.GONE);
                                            btnPlay.setVisibility(View.GONE);
                                            tv.setText("");
                                        } catch (Exception ex) {
                                        }
                                        totalDur = mediaPlayer.getDuration();
                                    }
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                },1000, t);
            } catch (Exception ex) {

            }
        }
    }

    class Sentence {
        String sentence_jp;
        String sentence_vi;
        String mp3;
        int learn;
        int type;
        int id;
    }

    List<Sentence> mListData;
    String prefix = "";
    DatabaseHandler mDB;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDB = new DatabaseHandler(getActivity());
        mListData = new ArrayList<>();
        type_id = 1;
        switch (AppData.currentLesson) {
            case 1:
                prefix = "k";
                break;
            case 2:
                prefix = "j";
                break;
            case 3:
                prefix = "i";
                break;
            case 5:
                prefix = "l";
                break;
            case 8:
                prefix = "m";
                break;
        }
        Map<String, String> map = new HashMap<>();
        map.put("lesson_id", AppData.currentLesson + "");
        APIUtils.getMarcoService().getListContentByLesson(map).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    try {
                        JsonArray jsonArray = response.body().getAsJsonArray("data");
                        if (jsonArray != null) {
                            Gson gson = new Gson();
                            String jsonOutput = jsonArray.toString();
                            Type listType = new TypeToken<List<Sentence>>() {
                            }.getType();
                            mListData = (List<Sentence>) gson.fromJson(jsonOutput, listType);
                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();
                        }
                    } catch (Exception ex) {
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        //
    }

    ListView mListView;
    ConversationAdapter mAdapter;
    Button btLeft1, btLeft2, btLeft3, btnPlay;
    ImageView role1, role2, role3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pharse_layout, container, false);
        mListView = view.findViewById(R.id.listConversation);
        mAdapter = new ConversationAdapter(this);
        mListView.setAdapter(mAdapter);
        btLeft1 = view.findViewById(R.id.btLeft1);
        btLeft2 = view.findViewById(R.id.btLeft2);
        btLeft3 = view.findViewById(R.id.btLeft3);
        role1 = view.findViewById(R.id.role1);
        role2 = view.findViewById(R.id.role2);
        role3 = view.findViewById(R.id.role3);
        ///
        recordbtn = (ImageButton) view.findViewById(R.id.mainButton);
        tv = (TextView) view.findViewById(R.id.tv);
        btnPlay = (Button) view.findViewById(R.id.btnPlay);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
        btnPlay.setVisibility(View.VISIBLE);
        recordbtn.setVisibility(View.GONE);
        tv.setText("マイクをタップして発音してください");
        ///
        isPaused = false;
        if (AppData.currentLesson == 1) {
            role1.setImageResource(R.drawable.girl1);
            role2.setImageResource(R.drawable.girl2);
            role3.setVisibility(View.GONE);
            btLeft1.setText("店員");
            btLeft2.setText("キムさん");
            btLeft3.setVisibility(View.GONE);
        }
        if (AppData.currentLesson == 3) {
            role1.setImageResource(R.drawable.girl1);
            role2.setImageResource(R.drawable.boy);
            role3.setVisibility(View.GONE);
            btLeft1.setText("サラさん");
            btLeft2.setText("店員");
            btLeft3.setVisibility(View.GONE);
            t=2000;
        }
        if (AppData.currentLesson == 5) {
            role1.setImageResource(R.drawable.leader);
            role2.setImageResource(R.drawable.girl1);
            role3.setImageResource(R.drawable.yamadasan);
            btLeft1.setText("リーダー");
            btLeft2.setText("ハンさん");
            btLeft3.setText("山田");
        }
        if (AppData.currentLesson == 8) {
            role1.setImageResource(R.drawable.boy);
            role2.setImageResource(R.drawable.leader);
            role3.setImageResource(R.drawable.boy2);
            btLeft1.setText("お客さん");
            btLeft2.setText("受け付け");
            btLeft3.setText("スタッフ");
            t=4000;
        }
        btLeft1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListView.setAdapter(mAdapter);
               mediaPlayer.pause();
                type_id = 1;
                mCurrentPosition = 0;
                recordbtn.setEnabled(true);
                countDownTimer.cancel();
                btnPlay.setVisibility(View.VISIBLE);
                tv.setText("マイクをタップして発音してください");
                btLeft2.setBackgroundResource(R.drawable.button_main);
                btLeft3.setBackgroundResource(R.drawable.button_main);
                btLeft1.setBackgroundResource(R.drawable.button_main_selected);
                isPaused = true;
            }
        });
        btLeft2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPaused == true) {
                    countDownTimer.cancel();
                }
                mListView.setAdapter(mAdapter);
                mCurrentPosition = -1;
                onNext(mCurrentPosition, t);
                btLeft2.setBackgroundResource(R.drawable.button_main_selected);
                btLeft1.setBackgroundResource(R.drawable.button_main);
                btLeft3.setBackgroundResource(R.drawable.button_main);
                type_id = 2;
                isPaused = true;
            }
        });
        btLeft3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPaused == true) {
                    countDownTimer.cancel();
                }
                mListView.setAdapter(mAdapter);
                mCurrentPosition = -1;
                onNext(mCurrentPosition, t);
                btLeft3.setBackgroundResource(R.drawable.button_main_selected);
                btLeft1.setBackgroundResource(R.drawable.button_main);
                btLeft2.setBackgroundResource(R.drawable.button_main);
                type_id = 3;
                isPaused = true;
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                recordbtn.setVisibility(View.VISIBLE);
                tv.setText("録音中、マイクをタップすると終了");
                btnPlay.setVisibility(View.GONE);
                Record();
            }
        });
        recordbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText("");
                progressBar.setVisibility(View.GONE);
                recordbtn.setVisibility(View.GONE);
                recordbtn.setEnabled(false);
                if (mRecorder != null) {
                    mRecorder.stop();
                    mRecorder.release();
                    mRecorder = null;
                }
                if (isPaused == true) {
                    countDownTimer.cancel();
                }

                onNext(mCurrentPosition,t);
                isPaused = true;
                //   Toast.makeText(getActivity(), "File is saved in JPLearningRecord Folder", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mRecorder = null;
        mediaPlayer=null;
    }
///////////////
//    @Override
//    public void onPause() {
//        super.onPause();
//        mRecorder = null;
//       mediaPlayer=null;
//    }
    ////////////////////
    class ConversationAdapter extends BaseAdapter {

        MediaPlayerInterface playerInterface;

        public ConversationAdapter(MediaPlayerInterface playerInterface) {
            this.playerInterface = playerInterface;
        }

        @Override
        public int getCount() {
            return mListData != null ? mListData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        public void setFocus(int position) {
            if (mCurrentPosition >= mListData.size())
                mCurrentPosition = 0;
            notifyDataSetChanged();
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final Sentence sentence = mListData.get(position);
            ViewHolder viewHolder = null;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(getActivity()).inflate(R.layout.item_lyric_speaker, parent, false);
                viewHolder.jpLine = (TextView) convertView.findViewById(R.id.line1);
                viewHolder.vnLine = (TextView) convertView.findViewById(R.id.line2);
                //    viewHolder.icon = convertView.findViewById(R.id.image_voice);
                viewHolder.avatar = convertView.findViewById(R.id.image_avatar);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                // viewHolder.icon.setImageResource(R.drawable.micro_black);
            }
            viewHolder.jpLine.setText(sentence.sentence_jp);
            viewHolder.vnLine.setText(sentence.sentence_vi);

            /////////////// align item in listview
            if (sentence.type != type_id) {
                RelativeLayout.LayoutParams layoutParams =
                        (RelativeLayout.LayoutParams) viewHolder.avatar.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_LEFT);
                //  layoutParams.addRule(RelativeLayout.RIGHT_OF,R.id.line1);
                viewHolder.avatar.setLayoutParams(layoutParams);
                viewHolder.jpLine.setGravity(Gravity.RIGHT);
                viewHolder.vnLine.setGravity(Gravity.RIGHT);

            } else {
                RelativeLayout.LayoutParams layoutParams =
                        (RelativeLayout.LayoutParams) viewHolder.avatar.getLayoutParams();
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                viewHolder.jpLine.setGravity(Gravity.LEFT);
                viewHolder.vnLine.setGravity(Gravity.LEFT);
            }
            ////////////////

            if (AppData.currentLesson == 1) {

                if (sentence.type == 1) {
                    viewHolder.avatar.setImageResource(R.drawable.girl1);
                    t=3500;
                } else if (sentence.type == 2) {
                    viewHolder.avatar.setImageResource(R.drawable.girl2);
                    t=2500;
                }
            }
            if (AppData.currentLesson == 2) {

                if (sentence.type == 1) {
                    viewHolder.avatar.setImageResource(R.drawable.girl1);
                } else if (sentence.type == 2) {
                    viewHolder.avatar.setImageResource(R.drawable.boy);
                } else if (sentence.type == 3) {
                    viewHolder.avatar.setImageResource(R.drawable.girl2);
                    t=4500;
                }
            }
            if (AppData.currentLesson == 3) {

                if (sentence.type == 1) {
                    viewHolder.avatar.setImageResource(R.drawable.girl1);
                    t=3500;
                } else if (sentence.type == 2) {
                    viewHolder.avatar.setImageResource(R.drawable.boy);
                }
            }
            if (AppData.currentLesson == 5) {
                if (sentence.type == 1) {
                    viewHolder.avatar.setImageResource(R.drawable.leader);
                    t=8200;
                } else if (sentence.type == 2) {
                    viewHolder.avatar.setImageResource(R.drawable.girl1);
                    t=8200;
                } else if (sentence.type == 3) {
                    viewHolder.avatar.setImageResource(R.drawable.yamadasan);
                    if(sentence.sentence_jp.equals("はい。"))
                        t=500;
                    else
                        t=3000;
                }
            }
            if (AppData.currentLesson == 8) {
                if (sentence.type == 1) {
                    viewHolder.avatar.setImageResource(R.drawable.boy);
                } else if (sentence.type == 2) {
                    viewHolder.avatar.setImageResource(R.drawable.leader);
                } else if (sentence.type == 3) {
                    viewHolder.avatar.setImageResource(R.drawable.boy2);
                    t=5000;
                }
            }
            if (mCurrentPosition == position) {
                viewHolder.jpLine.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                //           viewHolder.jpLine.setBackgroundResource(R.drawable.in_message);
            } else
                viewHolder.jpLine.setBackgroundColor(getResources().getColor(android.R.color.white));
            return convertView;
        }
    }

    class ViewHolder {
        TextView jpLine;
        TextView vnLine;
        ImageView icon, avatar;
        MusicWave animatedRecord;
    }

    class RawData {
        int startTime;
        int endTime;
        boolean isAuto; // 0 : person , 1: auto
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        killMediaPlayer();
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer.prepare();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void playAudio(String url) throws Exception {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
        int resID = getResources().getIdentifier(url, "raw", getActivity().getPackageName());
        mediaPlayer = MediaPlayer.create(getActivity(), resID);
        mediaPlayer.start();
        //     totalDur=mediaPlayer.getDuration();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_AUDIO_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    boolean permissionToRecord = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean permissionToStore = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (permissionToRecord && permissionToStore) {
                        Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    //Record voice
    public void Record() {
        String sep = File.separator; // Use this instead of hardcoding the "/"
        String newFolder = "JPLearningRecord";
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        if (CheckPermissions()) {
            File myNewFolder = new File(extStorageDirectory + sep + newFolder);
            myNewFolder.mkdir();
            outputFile = Environment.getExternalStorageDirectory().toString()
                    + sep + newFolder + sep + "Recording" + rnd.nextInt() + ".mp3";
//                                        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Recording"+
//                                                rnd.nextInt(10)+".3gp";
            recordbtn.setImageResource(R.drawable.voice_active);
            recordbtn.setEnabled(true);
            progressBar.setVisibility(View.VISIBLE);
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile(outputFile);
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                // Log.e("File", "prepare() failed");
            }
            mRecorder.start();
            //  Toast.makeText(getActivity(), "Recording Started", Toast.LENGTH_SHORT).show();

        } else {
            RequestPermissions();
        }
    }

    public boolean CheckPermissions() {
        int result = ContextCompat.checkSelfPermission(getActivity(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void RequestPermissions() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{RECORD_AUDIO, WRITE_EXTERNAL_STORAGE}, REQUEST_AUDIO_PERMISSION_CODE);
    }

//    // Get duration for music
//    public int getDuration(String url)throws Exception {
//        int duration = 1000;
//        int resID = getResources().getIdentifier(url, "raw", getActivity().getPackageName());
//        mediaPlayer = MediaPlayer.create(getActivity(), resID);
//            duration = mediaPlayer.getDuration();
//        return duration;
//        ////
//    }

}

