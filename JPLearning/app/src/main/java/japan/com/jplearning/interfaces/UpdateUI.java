package japan.com.jplearning.interfaces;

/**
 * Created by HK on 27/10/2017.
 */

public interface UpdateUI {
    public void onUpdate();
    public void updateText(int position);

}
