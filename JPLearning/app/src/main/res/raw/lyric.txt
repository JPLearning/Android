﻿『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi anh nhớ em, chỉ muốn gặp em ngay lúc này”
 だいじょうぶ もう泣かないで 私は風 あなたを包んでいるよ
Em không sao đâu anh đừng khóc nữa, em là cơn gió đang thổi qua anh đó thôi
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi, anh nhớ em, chỉ muốn gặp em ngay lúc này”
 ありがとう ずっと大好き 私は星 あなたを見守り続ける
Cảm ơn nhé,em sẽ mãi yêu anh, em nguyện là vì sao dõi theo anh từng bước
 あなたに出会えてよかった 本当に本当によかった
Em thật hạnh phúc khi gặp được anh, thực sự rất hạnh phúc.
 ここにもういれなくなっちゃった もう行かなくちゃ ホントゴメンね
“Em không thể ở đây thêm chút nào nữa, đến lúc phải đi rồi, thực sự xin lỗi anh
 私はもう一人で遠いところに行かなくちゃ
Em phải đi đến một nơi xa lắm
 どこへ？って聞かないで なんで？って聞かないで ホントゴメンね
Xin đừng hỏi em “Đến đâu?”, “Tại sao phải thế”, Em xin lỗi…
 私はもうあなたのそばにいられなくなったの
Em không thể ở bên cạnh anh nữa rồi”
 いつもの散歩道 桜並木を抜けてゆき
“Hoa anh đào vẫn rơi trên con đường chúng ta từng đi qua
 よく遊んだ川面の上の 空の光る方へと
Bên bờ sông, dưới bầu trời rộng lớn chúng ta hay cùng nhau chơi đùa
 もう会えなくなるけど 寂しいけど 平気だよ
Dù không thể gặp anh nữa, dù buồn lắm, nhưng mọi chuyện sẽ ổn thôi
 生まれてよかった ホントよかった あなたに出会ってよかった
Được sinh ra trên cuộc đời này, được gặp anh, chỉ thế thôi em đã thấy thực sự hạnh phúc”
 さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi anh nhớ em, chỉ muốn gặp em ngay lúc này”
だいじょうぶ もう泣かないで 私は風 あなたを包んでいるよ
Em không sao đâu anh đừng khóc nữa, em là cơn gió đang thổi qua anh đó thôi
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi, anh nhớ em, chỉ muốn gặp em ngay lúc này”
 ありがとう ずっと大好き 私は星 あなたを見守り続ける
Cảm ơn nhé,em sẽ mãi yêu anh, em nguyện là vì sao dõi theo anh từng bước
 あなたに出会えてよかった 本当に本当によかった
Em thật hạnh phúc khi gặp được anh, thực sự rất hạnh phúc.
 あなたの帰りを待つ午後 あなたの足音 何げないこと
“Tiếng bước chân trở về của anh buổi chiều hôm ấy
 私はそう、一番の喜びを知りました
Em biết đó là khoảng thời gian anh hạnh phúc nhất
 あなたが話してくれたこと 一日のこと いろいろなこと
Những khi anh kể chuyện, thật nhiều thật nhiều điều
 私はそう、一番の悲しみも知りました
Em cũng hiểu đó là lúc anh thật buồn…”
 それはあなたの笑顔 あなたの涙 その優しさ
“Nụ cười của anh, giọt nước mắt và cả sự ân cần ấy
 私の名を呼ぶ声 抱き締める腕 その温もり
Tiếng gọi tên em, vòng tay ôm em thật chặt và hơi ấm đó
 もう触れられないけど 忘れないよ 幸せだよ
Không thể chạm vào ký ức thêm 1 lần nào nữa nhưng em sẽ không bao giờ quên đâu, giây phút hạnh phúc ấy
 生まれてよかった ホントよかった あなたに出会ってよかった
Được sinh ra trên cuộc đời này, được gặp anh, chỉ thế thôi em đã thấy thực sự hạnh phúc
『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi anh nhớ em, chỉ muốn gặp em ngay lúc này
 だいじょうぶだよ ここにいる 私は春 あなたを抱く空
Em không sao đâu anh đừng khóc nữa, em là cơn gió đang thổi qua anh đó thôi
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
Hoa anh đào, hoa anh đào ơi, anh nhớ em, chỉ muốn gặp em ngay lúc này
 ありがとう ずっと大好き 私は鳥 あなたに歌い続ける
Cảm ơn nhé,em sẽ mãi yêu anh, em nguyện là cánh chim mãi hót cho anh nghe
 桜の舞う空の彼方 目を閉じれば心の中
Hoa anh đào vẫn bay ngập trời. Chỉ cần nhắm mắt, anh sẽ thấy em ở ngay trong tim….”
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi anh nhớ em, chỉ muốn gặp em ngay lúc này
 いいんだよ 微笑んでごらん 私は花 あなたの指先の花
Không sao đâu, cười lên đi, em là cánh hoa đậu trên ngón tay anh đó
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi, anh nhớ em, chỉ muốn gặp em ngay lúc này
 ありがとう ずっと大好き 私は愛 あなたの胸に
Cảm ơn nhé, em sẽ mãi yêu anh. Em là tình yêu trong chính trái tim anh…”
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi anh nhớ em, chỉ muốn gặp em ngay lúc này”
 だいじょうぶ もう泣かないで 私は風 あなたを包んでいるよ
Em không sao đâu anh đừng khóc nữa, em là cơn gió đang thổi qua anh đó thôi
 『さくら さくら 会いたいよ いやだ 君に今すぐ会いたいよ』
“Hoa anh đào, hoa anh đào ơi, anh nhớ em, chỉ muốn gặp em ngay lúc này”
 ありがとう ずっと大好き 私は星 あなたを見守り続ける
Cảm ơn nhé,em sẽ mãi yêu anh, em nguyện là vì sao dõi theo anh từng bước
 あなたに出会えてよかった 本当に本当によかった本当に本当によかった
Em thật hạnh phúc khi gặp được anh, thực sự rất hạnh phúc.”
